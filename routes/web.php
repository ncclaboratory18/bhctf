<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('/rules', 'HomeController@rules');

Route::get('/dontopenthis-yourpcwillexplode', function() {
	return view('flags.robots');
});

Route::get('/challs/contest/{cid?}', 'ChallsController@index')->name('contest.show');
Route::get('/rank/{cid?}', 'RankController@index')->name('rank.show');
Route::get('/true-rank/{cid?}', 'RankController@index2')->name('rank.show2');
Route::get('/challs', 'ChallsController@index')->name('challs.index');
Route::get('/challs/show', 'ChallsController@show')->name('challs.show');
Route::post('/challs/getChalls', 'ChallsController@getChalls');
Route::post('/challs/getSolves', 'ChallsController@getSolves');
Route::get('/challs/download/{id}', 'ChallsController@downloader');
Route::post('/challs/submit', 'ChallsController@submit');
// For Debugging
// Route::get('/challs/submit/{id}/{ans}', 'ChallsController@submitContestFlag');

Route::get('/contest', 'ContestController@index')->name('contest.index');
Route::post('/contest/enter', 'ContestController@enter');
Route::get('/contest/exit', 'ContestController@exit')->name('contest.exit');
Route::get('/contest/show', 'ContestController@show');
Route::get('/contest/download/{id}', 'ChallsController@downloader');
Route::post('/contest/submit/{id}', 'ContestController@submit');
Route::get('/contest/result/{id}', 'ContestController@result');

Route::get('/news', 'NewsController@index');

Route::get('/user/{name}', 'UserController@view');

Route::get('/writeup', 'WriteupController@index');
Route::get('/writeup/show/{id}', 'WriteupController@show');
Route::get('/writeup/read/{id}', 'WriteupController@read');
Route::get('/writeup/post', 'WriteupController@write');
Route::post('/writeup/comment', 'WriteupController@comment');
Route::post('/writeup/submit', 'WriteupController@submit');
Route::post('/writeup/like', 'WriteupController@like');
Route::post('/writeup/unlike', 'WriteupController@unlike');

Route::get('team', 'TeamController@index');
Route::get('team/info/{name}', 'TeamController@info');
Route::post('team/create', 'TeamController@create');
Route::get('team/delete/{id}', 'TeamController@delete');
Route::get('team/exit/{id}', 'TeamController@exit');
Route::post('team/join', 'TeamController@join');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function(){
	Route::get('/', 'DashboardController@index');
	Route::resource('/challs-organizer', 'ChallsController');
	Route::get('/users-organizer/verify', 'UsersController@verify');
	Route::resource('/users-organizer', 'UsersController');
	Route::resource('/news-organizer', 'NewsController');
	Route::resource('/contests-organizer', 'ContestsController');
	Route::resource('/teams-organizer', 'TeamsController');
});

Route::get('/unsubscribe', 'Admin\UsersController@unsubscribe');
Route::get('/upload/ktm', 'Admin\UsersController@newktm');
Route::post('/upload/ktm', 'Admin\UsersController@upnewktm');

Route::get('/logout', 'LoginController@destroy');

Route::get('/login', 'LoginController@index')->name('login');
Route::post('/login', 'LoginController@logmein');
Route::post('/login/forgot/username', 'LoginController@send');
Route::get('/login/forgot/reset', 'LoginController@resetview');
Route::post('/login/forgot/reset', 'LoginController@reset');

Route::get('/register', 'RegisController@index');
Route::post('/register', 'RegisController@create');
