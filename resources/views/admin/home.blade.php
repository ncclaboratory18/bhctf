@extends('layouts.admin')

@section('content')
	<div class="container">
		<form method="POST" action="/21232f297a57a5a743894a0e4a801fc3/create">
			{{ csrf_field() }}

		  	<div class="form-group">
		    	<label for="title">Title</label>
		    	<input type="text" class="form-control" id="title" name="title">
		  	</div>

		  	<div class="form-group">
		    	<label for="score">Score</label>
		    	<input type="number" class="form-control" id="score" name="score">
		  	</div>
		  	
		  	<div class="form-group">
		    	<label for="desc">Description</label>
		    	<textarea class="form-control" id="desc" name="desc" rows="5"></textarea>
		  	</div>
		  
		  	<button type="submit" class="btn btn-default">Submit</button>
		</form>
	</div>


@endsection()