@extends('layouts.master')

@section('content')
<div class="container">
	<div class="bs-docs-section">
		<div class="row">
			<div class="col-lg-6">
				<div class="well bs-component">
					<form class="form-horizontal" method="POST" action="/21232f297a57a5a743894a0e4a801fc3">
						<fieldset>
							<legend>Login</legend>

							{{ csrf_field() }}

							<div class="form-group">
								<label for="name" class="col-lg-2 control-label">Username</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="username" id="name">
								</div>
							</div>

							<div class="form-group">
								<label for="password" class="col-lg-2 control-label">Password</label>
								<div class="col-lg-10">
									<input type="password" class="form-control" name="password" id="password">
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button type="submit" class="btn btn-primary">Login</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection()