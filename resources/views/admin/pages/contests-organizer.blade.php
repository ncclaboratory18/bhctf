@extends("layouts.admin")

@section("content")
	<section class="content">
	    <div class="container-fluid">
		    <div class="row clearfix">
		    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    		<div class="card">
		    			<div class="header">
                            <h2>Contests <button class="btn btn-success pull-right" data-toggle="modal" data-target="#createModal">Create Contest</button></h2>
                        </div>

			    		<div class="body table-responsive">
			    			<table class="table table-hover">
			    				<thead>
			    					<tr>
			    						<th>Contest</th>
			    						<th>Starts At</th>
										<th>Ends At</th>
										<th>Freezed At</th>
										<th>Action</th>
			    					</tr>
			    				</thead>
			    				<tbody>
			    					@foreach($contest as $c)
			    						<tr>
			    							<td>{{ $c->name }}</td>
			    							<td>{{ $c->starts_at->toDayDateTimeString() }}</td>
											<td>{{ $c->ends_at->toDayDateTimeString() }}</td>
											<td>{{ $c->freezed_at->toDayDateTimeString() }}</td>
											<td><td><button class="btn btn-info edit" data-toggle="modal" data-target="#editModal" id="{{ $c->id }}">Edit</button></td></td>
			    						</tr>
			    					@endforeach
			    				</tbody>
			    			</table>
			    		</div>
		    		</div>
		    	</div>
		    </div>

		    {{-- Modals --}}

		    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="createModalLabel">Create Contest</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" enctype="multipart/form-data">

	                    		{{ csrf_field() }}

	                    		<div class="row clearfix">
	                    			<div class="col-md-12">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="name" class="form-control" placeholder="Name">
	                    					</div>
	                    				</div>
	                    			</div>
								</div>
								<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
                                            	<input type="datetime-local" class="form-control" placeholder="Starts At" name="starts_at">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
                                            	<input type="datetime-local" class="form-control" placeholder="Ends At" name="ends_at">
	                    					</div>
	                    				</div>
									</div>
									<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
                                            	<input type="datetime-local" class="form-control" placeholder="Freezed At" name="freezed_at">
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<br>
	                    		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                    		</form>
                        </div>
                    </div>
                </div>
			</div>
			<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="createModalLabel">Edit Challenge</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="" enctype="multipart/form-data" id="editForm">

	                    		{{ csrf_field() }}
	                    		{{ method_field('PUT') }}

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="name" id="name_edit" class="form-control" placeholder="Name">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
                                            	<input type="datetime-local" class="form-control" id="starts_at_edit" placeholder="Starts At" name="starts_at">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
                                            	<input type="datetime-local" class="form-control" id="ends_at_edit" placeholder="Ends At" name="ends_at">
	                    					</div>
	                    				</div>
									</div>
									<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
                                            	<input type="datetime-local" class="form-control" id="freezed_at_edit" placeholder="Freezed At" name="freezed_at">
	                    					</div>
	                    				</div>
	                    			</div>	
	                    		</div>

	                    		<br>
	                    		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                    		</form>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	</section>
@endsection


@section('custom_scripts')
	<script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

	<script>
		$.fn.modal.Constructor.prototype.enforceFocus = function () {
	    var $modalElement = this.$element;
	    $(document).on('focusin.modal', function (e) {
		        var $parent = $(e.target.parentNode);
		        if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length
		            // add whatever conditions you need here:
		            &&
		            !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
		            $modalElement.focus()
		        }
		    })
		};
	</script>

	<script>
		$(".edit").click(function() {
			var id = $(this).attr('id');
			$("#editForm").attr('action', 'contests-organizer/'+id);
			$.ajax({
			   url: 'contests-organizer/'+id,
			   type: 'GET',
			   error: function() {
			      console.log('error');
			   },
			   dataType: 'json',
			   success: function(data) {
				   console.log(data);
			      $("#name_edit").val(data.name);
			      $("#starts_at_edit").val(data.starts_at);
				  $("#ends_at_edit").val(data.ends_at);
				  $("#freezed_at_edit").val(data.freezed_at);
			   }
			});
		});
	</script>
@endsection