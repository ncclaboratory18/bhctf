@extends("layouts.admin")

@section('custom_css')
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}">
@endsection

@section("content")
	<section class="content">
	    <div class="container-fluid">
		    <div class="row clearfix">
		    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    		<div class="card">
		    			<div class="header">
                            <h2>Challenges<button class="btn btn-success pull-right" data-toggle="modal" data-target="#createModal">Create</button></h2>
                        </div>

			    		<div class="body table-responsive">
			    			<table class="table table-hover" id="table">
			    				<thead>
			    					<tr>
			    						<th>Contest</th>
			    						<th>Title</th>
			    						<th>Description</th>
			    						<th>Category</th>
			    						<th>Score</th>
			    						<th>Action</th>
			    					</tr>
			    				</thead>
			    				<tbody>
			    					@foreach($challs as $c)
			    						<tr id="{{ $c->id }}">
			    							@if($c->contest)
			    							<td>{{ $c->contest->name }}</td>
			    							@else
			    							<td>BrainHack</td>
			    							@endif
				    						<td>{{ $c->title }}</td>
				    						<td>{{ $c->desc }}</td>
				    						<td>{{ $c->category->name }}</td>
				    						<td>{{ $c->score }}</td>
				    						{{-- <td><a href="/21232f297a57a5a743894a0e4a801fc3/pages/challs-organizer/edit/{{ $c->id }}" class="btn btn-info">Edit</a></td> --}}
				    						<td><button class="btn btn-info edit" data-toggle="modal" data-target="#editModal" id="{{ $c->id }}">Edit</button></td>
					    					<td><button class="btn btn-danger delete" id="{{ $c->id }}">Delete</button></td>
			    						</tr>
			    					@endforeach
			    				</tbody>
			    			</table>
			    		</div>
		    		</div>
		    	</div>
		    </div>

		    {{-- Modals --}}

		    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="createModalLabel">Create Challenge</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" enctype="multipart/form-data">

	                    		{{ csrf_field() }}

	                    		<div class="row clearfix">
	                    			<div class="col-md-6">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="title" class="form-control" placeholder="Title">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-6">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="flag" class="form-control" placeholder="Flag">
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-sm-12">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<label>Description</label>
	                    						<textarea class="ckeditor" name="desc" id="desc">
	                    							
	                    						</textarea>
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="number" name="score" class="form-control" placeholder="Score">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<select name="category">
	                    							@foreach($category as $cat)
	                    								<option value="{{ $cat->id }}">{{ $cat->name }}</option>
	                    							@endforeach
	                    						</select>
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="source" class="form-control" placeholder="Source">
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
		                    				<div class="form-line">
		                    					<select name="contest">
		                    						<option value="0">BrainHack</option>
	                    							@foreach($contest as $c)
	                    								<option value="{{ $c->id }}">{{ $c->name }}</option>
	                    							@endforeach
	                    						</select>
		                    				</div>
		                    			</div>
	                    			</div>

	                    			<div class="col-md-6">
		                    			<div class="form-group">
		                    				<div class="form-line">
		                    					<input type="file" name="attach" class="form-control">
		                    				</div>
		                    			</div>
	                    			</div>
	                    		</div>

	                    		<br>
	                    		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                    		</form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="createModalLabel">Edit Challenge</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="" enctype="multipart/form-data" id="editForm">

	                    		{{ csrf_field() }}
	                    		{{ method_field('PUT') }}

	                    		<div class="row clearfix">
	                    			<div class="col-md-6">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="title" class="form-control" id="title_edit" placeholder="Title">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-6">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="flag" class="form-control" id="flag_edit" placeholder="Flag">
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-sm-12">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<label>Description</label>
	                    						<textarea class="ckeditor" name="desc" id="desc_edit">
	                    							
	                    						</textarea>
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="number" name="score" class="form-control" id="score_edit" placeholder="Score">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<select class="selectpicker" name="category_id">
	                    							@foreach($category as $cat)
	                    								<option value="{{ $cat->id }}" id="cat_{{ $cat->id }}">{{ $cat->name }}</option>
	                    							@endforeach
	                    						</select>
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="source" class="form-control" id="source_edit" placeholder="Source">
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
		                    				<div class="form-line">
		                    					<select class="selectpicker" name="contest_id">
		                    						<option value="0">BrainHack</option>
	                    							@foreach($contest as $c)
	                    								<option value="{{ $c->id }}">{{ $c->name }}</option>
	                    							@endforeach
	                    						</select>
		                    				</div>
		                    			</div>
	                    			</div>

	                    			<div class="col-md-6">
		                    			<div class="form-group">
		                    				<div class="form-line">
		                    					<input type="file" name="attach" class="form-control">
		                    				</div>
		                    			</div>
	                    			</div>
	                    		</div>

	                    		<br>
	                    		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                    		</form>
                        </div>
                    </div>
                </div>
			</div>
			
	    </div>
	</section>
@endsection

@section('custom_scripts')
	<script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

	<script>
		// $.fn.modal.Constructor.prototype.enforceFocus = function () {
	    // var $modalElement = this.$element;
	    // $(document).on('focusin.modal', function (e) {
		//         var $parent = $(e.target.parentNode);
		//         if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length
		//             // add whatever conditions you need here:
		//             &&
		//             !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
		//             $modalElement.focus()
		//         }
		//     })
		// };
	</script>

<script>
	$(".edit").click(function() {
		var id = $(this).attr('id');
		$("#editForm").attr('action', 'challs-organizer/'+id);
		$.ajax({
		   url: 'challs-organizer/'+id,
		   type: 'GET',
		   error: function() {
			  console.log('error');
		   },
		   dataType: 'json',
		   success: function(data) {
			  $("#title_edit").val(data.title);
			  $("#flag_edit").val(data.fakinflag);
			  CKEDITOR.instances['desc_edit'].setData(data.desc);
			  $("#score_edit").val(data.score);
			  $("#source_edit").val(data.source);
			  $('select[name=category_id]').val(data.category_id);
			  $('select[name=contest_id]').val(data.contest_id);
			  $('.selectpicker').selectpicker('refresh')
		   }
		});
	});
</script>

	<script>
		$(".delete").click(function() {
			if(confirm("Are you sure want to delete this?")) {
				var id = $(this).attr('id');
				$.ajax({
				  	url: 'challs-organizer/'+id,
				   	data: {
				   		_method: 'DELETE',
				   		_token: "{{ csrf_token() }}",
				   	},
				   	type: 'POST',
				   	error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);
                    },
				   	dataType: 'json',
				   	success: function(data) {
				   	 	if (data.status == 'success') {
					    	$("table#table tr#"+id).remove();
				   		}
				   		else console.log('An error occured');
				   	}
				});
			}
		});
	</script>
@endsection