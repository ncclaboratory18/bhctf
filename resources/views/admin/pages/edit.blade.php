@extends("layouts.admin")

@section("content")
	<section class="content">
	    <div class="container-fluid">
		    <div class="block-header">
		        <h2>Challenges</h2>
		    </div>

		    <div class="row clearfix">
		    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    		<div class="card">
		    			<div class="header">
		    				<h2>Edit Challenges</h2>
		    			</div>

		    			<div class="body">
				    		<form method="POST" enctype="multipart/form-data">

		                		{{ csrf_field() }}

		                		<div class="row clearfix">
		                			<div class="col-md-6">
		                				<div class="form-group">
		                					<div class="form-line">
		                						<input type="text" name="title" class="form-control" placeholder="Title" value="{{ $challs->title }}">
		                					</div>
		                				</div>
		                			</div>

		                			<div class="col-md-6">
		                				<div class="form-group">
		                					<div class="form-line">
		                						<input type="text" name="flag" class="form-control" placeholder="Flag" value="{{ $challs->fakinflag }}">
		                					</div>
		                				</div>
		                			</div>
		                		</div>

		                		<div class="row clearfix">
		                			<div class="col-sm-12">
		                				<div class="form-group">
		                					<div class="form-line">
		                						<label>Description</label>
		                						<textarea name="desc" id="desc">
		                							{{ $challs->desc }}
		                						</textarea>
		                					</div>
		                				</div>
		                			</div>
		                		</div>

		                		<div class="row clearfix">
		                			<div class="col-md-4">
		                				<div class="form-group">
		                					<div class="form-line">
		                						<input type="number" name="score" class="form-control" placeholder="Score" value="{{ $challs->score }}">
		                					</div>
		                				</div>
		                			</div>

		                			<div class="col-md-4">
		                				<div class="form-group">
		                					<div class="form-line">
		                						<select name="category">
		                							@foreach($category as $cat)
		                								<option value="{{ $cat->id }}">{{ $cat->name }}</option>
		                							@endforeach
		                						</select>
		                					</div>
		                				</div>
		                			</div>

		                			<div class="col-md-4">
		                				<div class="form-group">
		                					<div class="form-line">
		                						<input type="text" name="source" class="form-control" placeholder="Source" value="{{ $challs->source }}">
		                					</div>
		                				</div>
		                			</div>
		                		</div>

		                		<div class="row clearfix">
		                			<div class="col-md-4">
	                    				<div class="form-group">
		                    				<div class="form-line">
		                    					<select name="contest_id">
		                    						<option value="0">BrainHack</option>
	                    							@foreach($contest as $c)
	                    								<option value="{{ $c->id }}">{{ $c->name }}</option>
	                    							@endforeach
	                    						</select>
		                    				</div>
		                    			</div>
	                    			</div>
	                    			
		                			<div class="col-md-6">
		                    			<div class="form-group">
		                    				<div class="form-line">
		                    					<input type="file" name="attach" class="form-control">
		                    				</div>
		                    			</div>
		                			</div>
		                		</div>

		                		<br>
		                		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
		            		</form>
		            	</div>
		    		</div>
		    	</div>
		    </div>

		    {{-- Modals --}}

		    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="createModalLabel">Create Challenge</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" enctype="multipart/form-data">

	                    		{{ csrf_field() }}

	                    		<div class="row clearfix">
	                    			<div class="col-md-6">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="title" class="form-control" placeholder="Title">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-6">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="flag" class="form-control" placeholder="Flag">
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-sm-12">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<label>Description</label>
	                    						<textarea name="desc" id="desc">
	                    							
	                    						</textarea>
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="number" name="score" class="form-control" placeholder="Score">
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<select name="category">
	                    							@foreach($category as $cat)
	                    								<option value="{{ $cat->id }}">{{ $cat->name }}</option>
	                    							@endforeach
	                    						</select>
	                    					</div>
	                    				</div>
	                    			</div>

	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="source" class="form-control" placeholder="Source">
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>

	                    		<div class="row clearfix">
	                    			<div class="col-md-6">
		                    			<div class="form-group">
		                    				<div class="form-line">
		                    					<input type="file" name="attach" class="form-control">
		                    				</div>
		                    			</div>
	                    			</div>
	                    		</div>

	                    		<br>
	                    		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                    		</form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	</section>
@endsection()