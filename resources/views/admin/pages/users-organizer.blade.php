@extends('layouts.admin')

@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>Users <button class="btn-sm btn-success pull-right" id="mail_all" data-toggle="modal" data-target="#alertModal">Mail All Users</button></h2>
                        <a href="/generate">Generate All User Token</a>
					</div>
					<div class="body table-responsive">
						<table class="table table-hover" id="table">
							<thead>
								<tr>
									<th>#</th>
									<th>NRP</th>
									<th>Username</th>
									<th>Email</th>
									<th>Score</th>
									<th>Status</th>
									<th>Edit</th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $u)
								<tr id="{{ $u->id }}">
									<th scope="row">{{ ++$num }}</th>
									<td>{{ $u->nrp }}</td>
									<td>{{ $u->username }}</td>
									<td><a href="mailto:{{ $u->email }}">{{ $u->email }}</a></td>
									<td>{{ $u->score }}</td>
									<td class="status">
                                        @if($u->status == 0)
                                            Verified
                                        @elseif($u->status == 1)
                                            Not Verified
                                        @elseif($u->status == 2)
                                            Wrong KTM
                                        @elseif($u->status == 3)
                                            Banned
                                        @endif
                                    </td>
									<td>
										@if($u->status === 1)
										<button class="btn btn-primary verify" id="{{ $u->id }}">Verify</button>
										@endif
										<button class="btn btn-warning kirim" id="{{ $u->id }}" data-toggle="modal" data-target="#alertModal">Alert</button>
										<a href="users-organizer/{{ $u->id }}" class="btn btn-secondary">KTM</a>
                                        @if($u->status != 3)
								            <button class="btn btn-danger ban" id="{{ $u->id }}">Ban</button>
                                        @else
                                            <button class="btn btn-info unban" id="{{ $u->id }}">Unban</button>
                                        @endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="alertModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="createModalLabel">Alert User</h4>
            </div>
            <div class="modal-body">
            	<p id="ket">Enter the alert mail detail</p>
            	<p>Instant email: <button class="btn-sm btn-primary" id="wrong_ktm">Wrong KTM</button> <button class="btn-sm btn-danger" id="malicious">Malicious Behaviour</button></p>
                <form method="POST">

            		{{ csrf_field() }}

            		<input type="hidden" name="user_id" id="user_id" value="">

            		<div class="row clearfix">
            			<div class="col-md-6">
            				<div class="form-group">
            					<div class="form-line">
            						<input type="text" name="title" id="title" class="form-control" placeholder="Title">
            					</div>
            				</div>
            			</div>
            		</div>

            		<div class="row clearfix">
            			<div class="col-sm-12">
            				<div class="form-group">
            					<div class="form-line">
            						<label>Email Body</label>
            						<textarea class="ckeditor" name="content" id="content">
            							
            						</textarea>
            					</div>
            				</div>
            			</div>
            		</div>

            		<br>
            		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Send</button>
        		</form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('custom_scripts')
	<script>
        $(".ban").click(function() {
            if(confirm("Are you sure want to ban this user?")) {
                var id = $(this).attr('id');
                $.ajax({
                    url: 'users-organizer/'+id,
                    data: {
                        _method: 'DELETE',
                        _token: "{{ csrf_token() }}",
                        action: 'ban',
                    },
                    type: 'POST',
                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == 'success') {
                            location.reload();
                        }
                        else console.log('An error occured');
                    }
                });
            }
        });
    </script>

    <script>
        $(".unban").click(function() {
            if(confirm("Are you sure want to unban this user?")) {
                var id = $(this).attr('id');
                $.ajax({
                    url: 'users-organizer/'+id,
                    data: {
                        _method: 'DELETE',
                        _token: "{{ csrf_token() }}",
                        action: 'unban',
                    },
                    type: 'POST',
                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == 'success') {
                            location.reload();
                        }
                        else console.log('An error occured');
                    }
                });
            }
        });
    </script>

    <script>
    	$("#wrong_ktm").click(function(){
    		$("#title").val('Verifikasi KTM gagal');

    		CKEDITOR.instances['content'].setData('<p>Halo,</p><p>Terimakasih karena sudah menggunakan BrainHack CTF sebagai sarana belajar anda. Namun KTM yang anda submit pada saat registrasi tidak sesuai.</p><p>Untuk itu segera update KTM anda di <a href="http://brainhack.clayday.id/upload/ktm">sini</a>.</p><p>Jika dalam 4 hari sejak email ini dikirim KTM anda belum benar, maka hak akses anda pada BrainHack CTF akan dicabut.</p>Terima kasih.');
    	});

    	$("#malicious").click(function(){
    		$("#title").val('Tindakan malicious terdeteksi');

    		CKEDITOR.instances['content'].setData('<p>Halo,</p><p>Kami menemukan anda melakukan tindakan malicious terhadap layananan BrainHack CTF. Oleh karena itu, hak akses anda terhadap layanan ini dicabut. Kontak admin NCC untuk mengembalikan akun anda</p>');
    	});
    </script>

    <script>
        $(".kirim").click(function(){
            var id = $(this).attr('id');
            $("#user_id").val(id);
        });

    	$("#mail_all").click(function(){
			$("#user_id").val('all');
    	});
    </script>

    <script>
        $(".verify").click(function(){
            var id = $(this).attr('id');
            if(confirm("Are you sure want to verify this user?")) {
                $.ajax({
                    url: 'users-organizer/verify?id='+id,
                    type: 'GET',
                    dataType: 'json',
                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);
                    },
                    success: function(data) {
                        $('tr#'+id+' td.status').html('Verified');
                        $('tr#'+id+' button.verify').remove();
                    }
                });
            }
        });
    </script>
@endsection