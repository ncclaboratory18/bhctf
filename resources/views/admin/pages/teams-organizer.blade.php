@extends("layouts.admin")
@section('custom_css')
	<link rel="stylesheet" href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}">
@endsection
@section("content")
	<section class="content">
	    <div class="container-fluid">
		    <div class="row clearfix">
		    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    		<div class="card">
		    			<div class="header">
                            <h2>Teams <button class="btn btn-success pull-right" data-toggle="modal" data-target="#createModal">Create Teams</button></h2>
                        </div>

			    		<div class="body table-responsive">
			    			<table class="table table-hover">
			    				<thead>
			    					<tr>
			    						<th>Name</th>
			    						<th>Member</th>
										<th>TOKEN</th>
										<th>Action</th>
			    					</tr>
			    				</thead>
			    				<tbody>
			    					@foreach($team as $c)
			    						<tr>
			    							<td>{{ $c->name }}</td>
											{{-- <td>{{ dd(App\User::find($c->user_id)->get()->first()->username)}}</td> --}}
											<td>
											@foreach ($c->teamuser as $us)
												<p>{{ isset($us->user->username)?  $us->user->username : "Belum" }}</p>	
											@endforeach
											</td>
											<td>{{ $c->token }}</td>
											<td><button class="btn btn-info edit" data-toggle="modal" data-target="#editModal" id="{{ $c->id }}">Edit</button></td>
			    						</tr>
			    					@endforeach
			    				</tbody>
			    			</table>
			    		</div>
		    		</div>
		    	</div>
		    </div>

		    {{-- Modals --}}

		    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="createModalLabel">Create Contest</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" enctype="multipart/form-data">

	                    		{{ csrf_field() }}

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
	                    						<input type="text" name="team_name" class="form-control" placeholder="Name">
	                    					</div>
	                    				</div>
	                    			</div>
	                    			<div class="col-md-4">
	                    				<div class="form-group">
		                    				<div class="form-line">
		                    					<select class="selectpicker" name="user_id">
													{{-- <option value="NULL">No Leader</option> --}}
													{{-- {{dd($user)}} --}}
													@foreach($user as $u)
	                    								<option value="{{ $u->id }}">{{ $u->username }}</option>
	                    							@endforeach
	                    						</select>
		                    				</div>
		                    			</div>
	                    			</div>
	                    		</div>

	                    		<br>
	                    		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                    		</form>
                        </div>
                    </div>
                </div>
			</div>

			{{-- <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="createModalLabel">Edit Team</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="" enctype="multipart/form-data" id="editForm">

	                    		{{ csrf_field() }}
	                    		{{ method_field('PUT') }}

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
												<select class="selectpicker" name="user_id">
												@foreach($user as $u)
	                    								<option value="{{ $u->id }}">{{ $u->username }}</option>
	                    						@endforeach
	                    					</div>
	                    				</div>
	                    			</div>
	                    		</div>
								
								<button type="submit" class="btn btn-primary m-t-15 waves-effect">Edit</button>
	                    		<br>
                    		</form>
                        </div>
                    </div>
                </div>
			</div> --}}
			<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="createModalLabel">Edit Challenge</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="" enctype="multipart/form-data" id="editForm">

	                    		{{ csrf_field() }}
	                    		{{ method_field('PUT') }}

	              

	                    		<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
												<select class="selectpicker" name="user_id" id="user_id">
												@foreach($user as $u)
	                    								<option value="{{ $u->id }}">{{ $u->id }}-{{ $u->username }}</option>
	                    						@endforeach
	                    					</div>
										</div>
	                    			</div>
								</div>
								<div class="row clearfix">
	                    			<div class="col-md-4">
	                    				<div class="form-group">
	                    					<div class="form-line">
		                    					<input type="text" name="team_name" id="team_name" class="form-control" disabled>
		                    				</div>
										</div>
	                    			</div>
								</div>
								
	                    		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                    		</form>
                        </div>
                    </div>
                </div>
			</div>
			
	    </div>
	</section>
@endsection



@section('custom_scripts')
	<script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

	<script>
		$.fn.modal.Constructor.prototype.enforceFocus = function () {
	    var $modalElement = this.$element;
	    $(document).on('focusin.modal', function (e) {
		        var $parent = $(e.target.parentNode);
		        if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length
		            // add whatever conditions you need here:
		            &&
		            !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
		            $modalElement.focus()
		        }
		    })
		};
	</script>

	<script>
		$(".edit").click(function() {
			var id = $(this).attr('id');
			console.log(id);
			$("#editForm").attr('action', 'teams-organizer/'+id);
			$.ajax({
			   url: 'teams-organizer/'+id,
			   type: 'GET',
			   error: function() {
			      console.log('error');
			   },
			   dataType: 'json',
			   success: function(data) {
				   console.log(data);
				//    console.log(data);
				   $('select[id=user_id]').val(data.user_id);
				   $('#team_name').val(data.name);
				   $('.selectpicker').selectpicker('refresh')
			   }
			});
		});
	</script>
@endsection