@extends('layouts.admin')

@section('content')
	<section class="content">
	    <div class="container-fluid">
	        <div class="row clearfix">
	            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                <div class="card">
	                    <div class="header">
	                        <h2>News <button class="btn btn-success pull-right" data-toggle="modal" data-target="#createModal">Create</button></h2>
	                    </div>
	                    <div class="body table-responsive">
	                    	<table class="table table-striped" id="table">
	                    		<thead>
	                    			<tr>
	                    				<th>Date Posted</th>
	                    				<th>Title</th>
	                    				<th>Content</th>
	                    				<th>Action</th>
	                    			</tr>
	                    		</thead>
	                    		<tbody>
	                    			@foreach($news as $n)
	                    				<tr id="{{ $n->id }}">
	                    					<td>{{ $n->created_at }}</td>
	                    					<td>{{ $n->title }}</td>
	                    					<td>{{ $n->content }}</td>
	                    					<td><button class="btn btn-info edit" id="{{ $n->id }}" data-toggle="modal" data-target="#editModal">Edit</button></td>
                                            <td><button class="btn btn-danger delete" id="{{ $n->id }}">Delete</button></td>
	                    				</tr>
	                    			@endforeach
	                    		</tbody>
	                    	</table>
	                    </div>
	                </div>
	            </div>
            </div>
	    </div>
	</section>

	{{-- Modals --}}
	<div class="modal fade" id="createModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="createModalLabel">Create News</h4>
                </div>
                <div class="modal-body">
                    <form method="POST">

                		{{ csrf_field() }}

                		<div class="row clearfix">
                			<div class="col-md-6">
                				<div class="form-group">
                					<div class="form-line">
                						<input type="text" name="title" id="title" class="form-control" placeholder="Title">
                					</div>
                				</div>
                			</div>

                		</div>

                		<div class="row clearfix">
                			<div class="col-sm-12">
                				<div class="form-group">
                					<div class="form-line">
                						<label>Description</label>
                						<textarea class="ckeditor" name="content" id="content">
                							
                						</textarea>
                					</div>
                				</div>
                			</div>
                		</div>

                		<br>
                		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
            		</form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="createModalLabel">Edit News</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="" id="editForm">

                		{{ csrf_field() }}
                		{{ method_field('PUT') }}

                		<div class="row clearfix">
                			<div class="col-md-6">
                				<div class="form-group">
                					<div class="form-line">
                						<input type="text" name="title" id="title_edit" class="form-control" placeholder="Title">
                					</div>
                				</div>
                			</div>

                		</div>

                		<div class="row clearfix">
                			<div class="col-sm-12">
                				<div class="form-group">
                					<div class="form-line">
                						<label>Description</label>
                						<textarea class="ckeditor" name="content" id="content_edit">
                							
                						</textarea>
                					</div>
                				</div>
                			</div>
                		</div>

                		<br>
                		<button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
            		</form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_scripts')
	<script>
		$.fn.modal.Constructor.prototype.enforceFocus = function () {
	    var $modalElement = this.$element;
	    $(document).on('focusin.modal', function (e) {
		        var $parent = $(e.target.parentNode);
		        if ($modalElement[0] !== e.target && !$modalElement.has(e.target).length
		            // add whatever conditions you need here:
		            &&
		            !$parent.hasClass('cke_dialog_ui_input_select') && !$parent.hasClass('cke_dialog_ui_input_text')) {
		            $modalElement.focus()
		        }
		    })
		};
	</script>

	<script>
		$(".edit").click(function() {
			var id = $(this).attr('id');
			$("#editForm").attr('action', 'news-organizer/'+id);
			$.ajax({
			   url: 'news-organizer/'+id,
			   data: data,
			   type: 'GET',
			   error: function() {
			      console.log('error');
			   },
			   dataType: 'json',
			   success: function(data) {
			   		$("#title_edit").val(data.title);
			   		CKEDITOR.instances['content_edit'].setData(data.content);
			   }
			});
		});
	</script>

    <script>
        $(".delete").click(function() {
            if(confirm("Are you sure want to delete this?")) {
                var id = $(this).attr('id');
                $.ajax({
                    url: 'news-organizer/'+id,
                    data: {
                        _method: 'DELETE',
                        _token: "{{ csrf_token() }}",
                    },
                    type: 'POST',
                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == 'success') {
                            $("table#table tr#"+id).remove();
                        }
                        else console.log('An error occured');
                    }
                });
            }
        });
    </script>
@endsection