<!DOCTYPE html>
<html>
<head>
	<title>{{ $user->ktm }}</title>
</head>
<body>
	@if($user->ktm == 'none') 
		<p>No KTM submitted</p>
	@else
		<img src="/storage/images/{{ $user->ktm }}">
	@endif
</body>
</html>