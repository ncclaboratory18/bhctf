@extends('layouts.emails', ['user', $user])

@section('body')
	<h1>{!! $title !!}</h1>
	{!! $body !!}
@endsection