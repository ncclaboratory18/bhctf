@extends('layouts.emails', ['user' => $user])
@section('body')
	<h1>Thanks for Registering</h1>
	<hr>
	<p>Terimakasih telah mendaftar. Harap membaca <a href="http://brainhack.clayday.id/rules">peraturan</a> terlebih dahulu.</p>
	<p>Happy hacking!</p>
@endsection