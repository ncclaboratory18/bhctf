<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>BrainHack CTF | Presented by Net Centric Computing Lab.</title>

<link href="/css/bootstrap.css" rel="stylesheet">

<link href="/css/4-col-portfolio.css" rel="stylesheet">

<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >