<footer id="footer">
  <div class="copyright">
    <div class="container">
      <ul class="pull-right nomargin list-inline mobile-block">
        <li><a href="http://if.its.ac.id">Informatika ITS</a></li>
        <li>&bull;</li>
        <li><a href="http://kbj.if.its.ac.id">Lab. Komputasi Berbasis Jaringan</a></li>
      </ul>
      &copy; BrainHack CTF 2.0 | Net Centric Computing Laboratory
    </div>
  </div>
</footer>