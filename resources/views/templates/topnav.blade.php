@if (!request()->session()->has('current_contest'))
<header id="topNav" class="clearfix dark">
    <div class="container">
        <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
            <i class="fa fa-bars" style="color: white"></i>
        </button>


        <!-- Logo -->
        <a class="logo pull-left" href="/">
            <img src="{{ asset('images/20x5-trans.png')}}" alt="" />
        </a>

        <div class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark">
            <nav class="nav-main">

                <ul id="topMain" class="nav nav-pills nav-main">
                    <li>
                        <a href="/"><i class="fa fa-home"></i> Home</a>
                    </li>
                    @if (auth()->check())
                    <li>
                        <a href="/challs"><i class="fa fa-edit"></i> Challenges</a>
                    </li>
                    <li>
                        <a href="{{ Route('rank.show') }}"><i class="fa fa-tasks"></i> Scoreboard</a>
                    </li>
                    <li>
                        <a href="/contest"><i class="fa fa-trophy"></i> Contests</a>
                    </li>
                    <li>
                        <a href="/writeup"><i class="fa fa-book"></i> Writeup</a>
                    </li>
                    <li>
                        <a> | </a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user-circle-o fa-lg"></i> </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="dropdown">
                                <a href="/team"><i class="fa fa-users"></i> My Team</a>
                            </li>
                            @if (auth()->user()->permission == 1)
                            <li class="dropdown">
                                <a href="/admin"><i class="fa fa-gears"></i> Admin</a>
                            </li>
                            @endif
                            <li class="divider"></li>
                            <li class="dropdown">
                                <a href="/logout"><i class="fa fa-arrow-circle-o-left"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="/login"><i class="fa fa-arrow-circle-right"></i> Login</a>
                    </li>
                    <li>
                        <a href="/register"><i class="fa fa-user-plus"></i> Register</a>
                    </li>
                    @endif
                </ul>
            </nav>
        </div>

    </div>
</header>
@else
<header id="topNav" class="clearfix dark">
    <div class="container">
        <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
            <i class="fa fa-bars" style="color: white"></i>
        </button>

        <a class="logo pull-left" href="{{ Route('contest.index') }}">
            <p style="font-size: 200%">{{ request()->session()->get('current_contest')->name }}</p>
        </a>


        <div class="navbar-collapse pull-right nav-main-collapse collapse submenu-dark">
            <nav class="nav-main">

                <ul id="topMain" class="nav nav-pills nav-main">
                    <li>
                        <a href="{{ Route('contest.show', $conid) }}"><i class="fa fa-edit"></i> Challenges</a>
                    </li>
                    <li>
                        <a href="{{ Route('rank.show', $conid) }}"><i class="fa fa-tasks"></i> Scoreboard</a>
                    </li>
                    <li>
                        <a href="{{ Route('contest.exit') }}"><i class="fa fa-arrow-circle-o-left"></i> Exit Contest</a>
                    </li>
                </ul>
            </nav>
        </div>

    </div>
</header>
@endif
