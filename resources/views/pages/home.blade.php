<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<title>BrainHack CTF | Network Security Learning Portal</title>
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="{{ asset('2.0/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="{{ asset('2.0/assets/css/essentials.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('2.0/assets/css/layout.css')}}" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="{{ asset('2.0/assets/css/header-1.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('2.0/assets/css/color_scheme/green.css')}}" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>

	<body class="smoothscroll enable-animation">

		<!-- wrapper -->
		<div id="wrapper">

			<div id="header" class="sticky transparent noborder clearfix">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="full-container">
						<!-- Logo -->
						<a class="logo pull-left" href="/">
							<img src="{{ asset('images/transparan.png') }}" alt="" /> <!-- light logo -->
						</a>
						<a class="logo pull-right" href="/">
							<img src="{{ asset('images/ncc.png') }}" alt="" /> <!-- light logo -->
						</a>
					</div>
				</header>
				<!-- /Top Nav -->

			</div>




			<!-- SLIDER -->
			<section id="slider" class="fullheight" style="background:url('{{ asset('2.0/assets/images/demo/particles_bg.jpg')}}')">
				<span class="overlay dark-2"><!-- dark overlay [0 to 9 opacity] --></span>

				<canvas id="canvas-particle" data-rgb="156,217,249"><!-- CANVAS PARTICLES --></canvas>

				<div class="display-table">
					<div class="display-table-cell vertical-align-middle">

						<div class="container text-center">


							<h1 class="nomargin wow fadeInUp" data-wow-delay="0.4s">
								Welcome To BrainHack CTF
								<br>

								<span class="rotate " data-animation="flip" data-speed="1500">
									2.0
								</span>

								<p class="lead font-lato size-30 wow fadeInUp" data-wow-delay="0.7s">
									Proudly Presented by
									<a href="http://kbj.if.its.ac.id"><span class="theme-color weight-400 font-style-italic">Net Centric Computing</span></a> Lab.
								</p>
								<p class="lead font-lato size-20 wow fadeInUp">
									Currently open only for ITS students
								</p>
							</h1>

							@if (auth()->check())
								<a class="btn btn-default btn-lg" href="{{ Route('contest.show') }}">CHALLENGES</a>
							@else
								<a class="btn btn-default btn-lg" href="/login">LOGIN</a>
								<a class="btn btn-default btn-lg" href="/register">REGISTER</a>
							@endif

						</div>

					</div>
				</div>

			</section>
			<!-- /SLIDER -->
		</div>
		<!-- /wrapper -->


		<!-- SCROLL TO TOP -->
		<!-- <a href="#" id="toTop"></a> -->


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->


		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = '2.0/assets/plugins/';</script>
		<script type="text/javascript" src="{{ asset('2.0/assets/plugins/jquery/jquery-2.2.3.min.js')}}"></script>

		<script type="text/javascript" src="{{ asset('2.0/assets/js/scripts.js')}}"></script>

		<!-- PARTICLE EFFECT -->
		<script type="text/javascript" src="{{ asset('2.0/assets/plugins/effects/canvas.particles.js')}}"></script>
	</body>
</html>
