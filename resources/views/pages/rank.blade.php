@extends('layouts.master')

@section('title', 'Scoreboard')

@section('content')

	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Username</th>
							<th>Score</th>
						</tr>
					</thead>
					<tbody>
						@foreach($teams as $t)
							<tr>
								<td>{{ ++$rank }}</td>
								<td><a href="user/{{ $t->username }}">{{ $t->username }}</a></td>
								<td>{{ $t->score }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>

				{{ $teams->render() }}
			</div>
		</div>
	</div>
@endsection()