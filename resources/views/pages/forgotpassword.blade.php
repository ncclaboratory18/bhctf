@extends('layouts.master')

@section('content')
<div class="container">
	<div class="bs-docs-section">
		<div class="row">
			<div class="col-lg-6">
				<div class="well bs-component">
					<form class="form-horizontal" method="POST" action="/login/forgot/reset">
						<fieldset>
							<legend>Reset Password</legend>

							{{ csrf_field() }}

							<div class="form-group">
								<label for="password" class="col-lg-2 control-label">New Password</label>
								<div class="col-lg-10">
									<input type="password" class="form-control" name="password" id="password">
								</div>
							</div>

							<div class="form-group">
								<label for="password_confirmation" class="col-lg-2 control-label">Confirm Password</label>
								<div class="col-lg-10">
									<input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
								</div>

							</div>

							<input type="hidden" name="i" value="{{ request('i') }}">
							<input type="hidden" name="token" value="{{ request('token') }}">

							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button type="submit" class="btn btn-primary">Reset Password</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection