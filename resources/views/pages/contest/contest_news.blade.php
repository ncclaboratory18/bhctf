@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="bs-docs-section">
			<div class="row">
				<h1>News</h1>
				@foreach($news as $n)
					<hr>
					<h2>{{ $n->title }}</h2>
					<small><p style="font-style: italic">{{ $n->poster }}, at {{ $n->created_at->toFormattedDateString() }}</p></small>
					<p>{{ $n->content }}</p>
				@endforeach
			</div>
		</div>
	</div>
@endsection