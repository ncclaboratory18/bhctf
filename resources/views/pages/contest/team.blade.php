@extends('layouts.master')

@section('title', 'My Team')

@section('content')
	<div class="container">

	<div class="row">
		<div class="col-md-6">
			<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#createTeam">Create Team</button>
			<button class="btn btn-info pull-right" href="#joinTeam" data-toggle="collapse" >Join Team</button>
			

			@if($team->count())
			<table class="table table-stripped">
				<thead>
					<tr>
						<th>Team</th>
						<th>Token</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($team as $t)
					<tr>
							<td><a href="/team/info/{{ $t->name }}">{{ $t->name }}</a>@if($t->user_id === $user->id) <span class="label label-info">You're Leader</span>@endif</td>
							<td>{{ $t->token }}</td>
							<td>
								@if($t->user_id === $user->id)
								<button onclick="deleteTeam({{$t->id}})" class="btn btn-danger btn-sm">Delete Team</button>
								@else
								<button onclick="exitTeam({{$t->id}})" class="btn btn-danger btn-sm">Exit Team</button>
								@endif
							</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
			<p>You have not joined any team</p>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			@endif
			<br>
			<br>
			<br>
		</div>

		<div class="col-md-6">
			<div class="panel panel-default collapse" id="joinTeam">
				<div class="panel-heading">
					<h2 class="panel-title">Join Team</h2>
				</div>
				<div class="panel-body">
					<form method="POST" action="/team/join" autocomplete="off">
						{{ csrf_field() }}

						<div class="col-md-10">
							<input type="text" name="token" placeholder="Team token" class="form-control">
						</div>
						<div class="col-md-2">
							<input type="submit" name="submit" value="Join" class="btn btn-primary">
						</div>
					</form>					
				</div>
			</div>		
		</div>
	</div>


	<div class="modal fade" id="createTeam" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">Create Team</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
		        <form method="POST" action="/team/create">
		        	{{ csrf_field() }}
		        	<div class="form-group">
						<div class="col-md-8">
							<input type="text" name="team_name" placeholder="Team name" class="form-control">
						</div>
						<div class="col-md-4">
							<input type="submit" name="submit" value="Submit" class="btn btn-primary">
						</div>
		        	</div>
		        </form>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>

	</div>

	<script type="text/javascript">
		function deleteTeam(id) {
		    var ask = window.confirm("Are you sure you want to delete this team? (All team data will be lost)");
		    if (ask) {
		        document.location.href = "/team/delete/"+id;
		    }
		}
		function exitTeam(id) {
		    var ask = window.confirm("Are you sure you want to exit this team?");
		    if (ask) {
		        document.location.href = "/team/exit/"+id;
		    }
		}
	</script>
@endsection