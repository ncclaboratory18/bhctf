@extends('layouts.master')

@section('title', 'Contests')

@section('content')
	<div class="row">
		<div class="col-md-6">
			<h3>Currently running</h3>
			@if(!$run->count())
			<p>There are currently no running contest</p>
			@else
			@foreach($run as $c)
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h2 class="panel-title">{{ $c->name }}</h2>
							</div>

							<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<img src="{{ $c->image_path }}" style="max-height: 150px; max-width: 150px">
								</div>

								<div class="col-md-8">
									<div class="row">
										<div class="col-md-5">
											<p>{{ $c->starts_at->toDayDateTimeString() }}</p>
										</div>

										<div class="col-md-2">
											<h3>→</h3>
										</div>

										<div class="col-md-5">
											<p>{{ $c->ends_at->toDayDateTimeString() }}</p>
										</div>
									</div>

									<div class="progress">
										<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="{{ ($now->diffInSeconds($c->starts_at) / $c->ends_at->diffInSeconds($c->starts_at)) * 100 }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ ($now->diffInSeconds($c->starts_at) / $c->ends_at->diffInSeconds($c->starts_at)) * 100 }}%" min-width: 2em;>
											{{ $c->ends_at->diffForHumans($now, true) }} left!
										</div>
									</div>

									<div class="pull-right">
										@if(!$team)
											<button class="btn btn-primary" data-toggle="modal" data-target="#createTeam" id="enter" value="{{ $c->id }}">Create Team</button>
											<button class="btn btn-primary" data-toggle="modal" data-target="#joinTeam" id="enter" value="{{ $c->id }}">Join Team</button>
										@else
											<button class="btn btn-primary enter" data-toggle="modal" data-target="#enterContest" id="enter" value="{{ $c->id }}">Enter Contest</button>
										@endif
									</div>
								</div>
							</div>
							</div>

						</div>
					</div>
				</div>
			@endforeach
			@endif
		</div>

		<div class="col-md-6">
			@if($past->count())
				<h3>Past Contests</h3>
				@foreach($past as $c)
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading panel-heading-transparent">
									<h2 class="panel-title">{{ $c->name }}</h2>
								</div>

								<div class="panel-body">
								<div class="row">
									<div class="col-md-4">
										<img src="{{ $c->image_path }}" style="max-height: 150px; max-width: 150px">
									</div>

									<div class="col-md-8 text-center">
										<div class="row">
											<h3>Contest has ended</h3>
										</div>

										<div class="row">
											<a href="{{ Route('contest.show', $c->id) }}" class="btn btn-default">Challenges</a>
											<a href="/writeup/{{ $c->id }}" class="btn btn-default">Writeup</a>
											<a href="{{ Route('rank.show', $c->id) }}" class="btn btn-default">Scoreboard</a>
										</div>
									</div>
								</div>
								</div>

							</div>
						</div>
					</div>
				@endforeach
			@else
			<p>There are currently no past contest</p>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<h3>Upcoming Contests</h3>
			@if($up->count())
				@foreach($up as $c)
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-warning">
								<div class="panel-heading">
									<h2 class="panel-title">{{ $c->name }}</h2>
								</div>

								<div class="panel-body">
								<div class="row">
									<div class="col-md-4">
										<img src="{{ $c->image_path }}" style="max-height: 150px; max-width: 150px">
									</div>

									<div class="col-md-8">
										<div class="row">
											<div class="col-md-5">
												<p>{{ $c->starts_at->toDayDateTimeString() }}</p>
											</div>

											<div class="col-md-2">
												<h3>→</h3>
											</div>

											<div class="col-md-5">
												<p>{{ $c->ends_at->toDayDateTimeString() }}</p>
											</div>
										</div>

										<div class="pull-right">
											@if(!$team)
												<button class="btn btn-primary" data-toggle="modal" data-target="#createTeam" id="enter" value="{{ $c->id }}">Create Team</button>
												<button class="btn btn-primary" data-toggle="modal" data-target="#joinTeam" id="enter" value="{{ $c->id }}">Join Team</button>
											@endif
										</div>
									</div>
								</div>
								</div>

							</div>
						</div>
					</div>
				@endforeach
			@else
			<p>There are currently no upcoming contest</p>
			@endif
		</div>
	</div>

	<div class="modal fade" id="joinTeam" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">Join Team</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
		        <form method="POST" action="/team/join" autocomplete="off">
		        	{{ csrf_field() }}
		        	<div class="form-group">
						<div class="col-md-10">
							<input type="text" name="token" placeholder="Team token" class="form-control">
						</div>
						<div class="col-md-2">
							<input type="submit" name="submit" value="Join" class="btn btn-primary">
						</div>
		        	</div>
		        </form>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="createTeam" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">Create Team</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
		        <form method="POST" action="/team/create">
		        	{{ csrf_field() }}
		        	<div class="form-group">
						<div class="col-md-10">
							<input type="text" name="team_name" placeholder="Team Name" class="form-control">
						</div>
						<div class="col-md-2">
							<input type="submit" name="submit" value="Submit" class="btn btn-primary">
						</div>
		        	</div>
		        </form>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="enterContest" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">Select your team</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="row">
		        <form method="POST" action="/contest/enter">
		        	{{ csrf_field() }}
		        	<div class="form-group">
						<label for="select" class="col-lg-2 control-label">Select Team</label>
					    <div class="col-lg-8">
					        <select class="form-control" id="select" name="tid">
					          @foreach(auth()->user()->team as $t)
					          <option value="{{ $t->id }}">{{ $t->name }}</option>
					          @endforeach
					        </select>
					    </div>
					    <input type="hidden" name="cid" id="cid" value="">
						<div class="col-md-2">
							<input type="submit" name="submit" value="Enter" class="btn btn-primary">
						</div>
		        	</div>
		        </form>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>
@endsection

@section('includes-scripts')
	@parent

	<script>
		$(".enter").click(function(){
			$("#cid").val($(this).val());
		});
	</script>
@endsection
