@extends('layouts.master')

@section('title', 'Scoreboard')

@section('content')
	<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Total Skor</th>
			</tr>
			@foreach($teams as $t)
				@if(!$t->user_id == 1)
				<tr>
					<td>{{ ++$rank }}</td>
					<td>
						<a href="/team/info/{{ $t->team->name }}"></a>{{ $t->team->name }}
						@if ($rank == 1)
							<i class="fa fa-trophy" style="color: #C98910"></i>
						@elseif ($rank == 2)
							<i class="fa fa-trophy" style="color: #A8A8A8"></i>
						@elseif ($rank == 3)
							<i class="fa fa-trophy" style="color: #965A38"></i>
						@endif
					</td>
					<td>{{ $t->total }}</td>
				</tr>
				@endif
			@endforeach
		</table>
	</div>
@endsection()