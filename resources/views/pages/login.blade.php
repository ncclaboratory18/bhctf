@extends('layouts.master')

@section('title', 'Login')

@section('content')
<div class="row">
	<div class="col-md-6 col-md-offset-3">

		<div class="box-static box-border-top padding-30">
			<div class="box-title margin-bottom-30">
				<h2 class="size-20">Login </h2>
			</div>

			<form class="nomargin" method="post" action="/login">
				{{ csrf_field() }}

				<div class="clearfix">
					
					<div class="form-group">
						<input type="text" name="username" class="form-control" placeholder="Username" required="" autofocus="autofocus">
					</div>
					
					<!-- Password -->
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Password" required="">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<!-- Inform Tip -->                                        
						<div class="form-tip pt-20">
							<a class="no-text-decoration size-13 margin-top-10 block" data-toggle="modal" data-target="#forgotModal" >Forgot Password?</a>
						</div>
					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-6 text-right">
						<button class="btn btn-primary">LOG IN</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="forgotModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Forgot Password</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
	        <form method="POST" action="/login/forgot/username" autocomplete="off">
	        	
	        	{{ csrf_field() }}

	        	<div class="form-group">
					<div class="col-md-10">
						<input type="text" name="username" placeholder="Enter your username" class="form-control">
					</div>
					<div class="col-md-2">
						<input type="submit" name="submit" value="OK" class="btn btn-primary form-control">
					</div>
	        	</div>
	        </form>
      	</div>
      </div>
    </div>
  </div>
</div>
@endsection