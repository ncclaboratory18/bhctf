@extends('layouts.master')

@section('content')
<div class="container">
	<h1>Rules</h1>
	<hr>

	<h2>Learning Portal</h2>
	<p>BrainHack CTF learning portal adalah tempat belajar para mahasiswa Informatika ITS yang mempunyai ketertarikan pada cyber security. Portal ini menyediakan kumpulan soal-soal Capture The Flag jeopardy yang dikompilasi oleh admin. Sumber soal dapat dilihat saat peserta sudah menyelesaikan soal tersebut</p>

	<h2>Read This, Noob</h2>
	<p>Hey noob, this is LEARNING PORTAL. Ini bukan tempat kalian untuk pamer skor tinggi dan menyimpan sendiri ilmunya. Kita adalah pemula disini, pembelajar. Jika kamu berhasil solve suatu challenge, sebarkan caranya, bukan flagnya!</p>

	<h2>Rules</h2>
	<ol>
		<li>Portal ini dikhususkan untuk civitas akademika Informatika ITS, yang dibuktikan dengan KTM</li>
		<li>Dilarang melakukan tindakan malicious terhadap server seperti flooding, DDOS, guessing flag (bruteforce), ataupun memakai tool scanner yang bertujuan merusak layanan ini</li>
		<li>Dilarang berbagi flag pada peserta lain</li>
		<li>Dikarenakan portal ini adalah kumpulan soal-soal, maka format flag bisa berbeda-beda</li>
		<li>Apabila melanggar peraturan ini, maka hak akses anda terhadap web ini akan dicabut</li>
		<li>Ingat-ingat peraturan dasar disini : We are all noob</li>
	</ol>

	<br>
</div>
@endsection