@extends('layouts.master')

@section('content')
	<div class="container">

		<h1>Challenges Completed by {{ $user->username }}</h1>
		<hr>
	
		<table class="table table-striped table-hover">
			<tr>
				<th>No</th>
				<th>Challenge</th>
				<th>Completed at</th>
			</tr>
			@foreach($user->completed as $com)
				<tr>
					<td>{{ ++$num }}</td>
					<td>{{ $com->challs->title }}</td>
					<td>{{ $com->created_at->toDayDateTimeString() }}</td>
				</tr>
			@endforeach
		</table>

		<h3>Progress</h3>

		<div class="progress progress-striped active">
		  <div class="progress-bar" style="width: {{ ($num/$totchalls)*100 }}%"></div>
		</div>
		<p>Completed ({{ $num }} / {{ $totchalls }})</p>

	</div>
@endsection