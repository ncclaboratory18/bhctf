@extends('layouts.master')

@section('content')
<div class="container">
	<div class="bs-docs-section">
		<div class="row">
			<div class="col-lg-6">
				<div class="well bs-component">
					<form class="form-horizontal" method="POST" enctype="multipart/form-data">
						<fieldset>
							<legend>KTM Upload</legend>

							{{ csrf_field() }}

							<div class="form-group">
								<label for="password" class="col-lg-2 control-label">KTM</label>
								<div class="col-lg-10">
									<input type="file" class="form-control" name="ktm" id="ktm">
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button type="submit" class="btn btn-primary">Upload</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection