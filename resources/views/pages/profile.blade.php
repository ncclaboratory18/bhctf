@extends('layouts.master')

@section('custom_css')
<link rel="stylesheet" href="/css/team.css">
@endsection

@section('content')
	<div class="container">

		<h1>Edit Profile</h1>
		<hr>
	
		<div class="row">
			<div class="column">
				<div class="card">
					<img src="/images/user/{{ $user->profpic }}" style="width:100%">
					<br>
					<br>
					<div class="container">
						<form method="POST" action="/profile/change" enctype="multipart/form-data">

							{{ csrf_field() }}

							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label for="profpic">Change Profile Picture</label>
										<input type="file" name="profpic" id="profpic" class="form-control">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<input type="submit" name="submit" class="btn btn-primary" value="Save Change">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
@endsection