@extends('layouts.master')

@section('title', 'Challenges')

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">

    <div class="row">

        <div class="col-md-9 col-sm-9">
            @foreach ($category as $cat)
                <div class="row">
                    <div class="col-md-12">
                        @if ($cat->challs($conid)->count() > 0)
                            <div class="heading-title heading-dotted text-center">
                                <h3><span>{{ $cat->name }}</span></h3>
                            </div>
                        
                            @foreach ($cat->challs($conid) as $chall)
                                @if (in_array($chall->id, $done))
                                <button data-toggle="modal" data-target="#challsModal" class="btn btn-3d btn-xlg btn-green challs completed" id="{{ $chall->id }}">
                                    {{ $chall->title }}
                                    <span class="block font-lato">{{ $chall->score }}</span>
                                </button>
                                @else
                                <button data-toggle="modal" data-target="#challsModal" class="btn btn-3d btn-xlg btn-brown challs" id="{{ $chall->id }}">
                                    {{ $chall->title }}
                                    <span class="block font-lato">{{ $chall->score }}</span>
                                </button>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <br>
            @endforeach
        </div>

         <div class="col-md-3 col-sm-3">
            <!-- side navigation -->
            @if ($conid)
            <div class="side-nav">
                <p>Viewing challenges from past contest. Solving these challenges won't give you any score.</p>
            </div>
            @endif


            <div class="side-nav">

                <div class="side-nav-head">
                    <button class="fa fa-bars"></button>
                    <h4>Top 10</h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Username</th>
                                <th>Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($top10 as $top)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $top->username }}</td>
                                    <td>{{ $top->score }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /side navigation -->

        </div>
        
        <div id="modals">
            <div id="challsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="challsModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="challsModalLabel"></h4>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body" id="tabs">
                            <ul class="nav nav-tabs nav-bottom-border">
                                <li id="challsTab" class="active"><a href="#challenge" data-toggle="tab">Challenge</a></li>
                                <li id="solvesTab" value=""><a href="#solves" data-toggle="tab">Solves</a></li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="challenge">
                                    <div class="text-center">
                                        <h4 id="title"></h4>
                                        <p id="score"></p>
                                    </div>
                                    <p id="desc"></p>

                                    <div id="attachments">
                                        
                                    </div>

                                    <br>
                                    <form class="form" id="flagForm" onsubmit="return false">
                                        <input type="hidden" name="challs_id" id="challs_id">
                                        <div class="row">
                                            <div class="col-md-10 col-sm-10">
                                                <input type="text" name="ans" id="flag" class="form-control" placeholder="Flag:">
                                            </div>

                                            <div class="col-md-2 col-sm-2">
                                                <button type="button" class="btn btn-primary" id="submitFlag">Submit</button>
                                            </div>
                                        </div>
                                    </form>

                                    <button type="button" class="btn btn-success btn-lg btn-block" data-dismiss="modal" id="solvedNotif" style="display: none"><i class="et-flag"></i> You already solved this!</button>
                                    <button class="btn btn-danger btn-lg btn-block disabled collapse" id="flagFalse"><i class="fa fa-frown-o"></i> Wrong, Try Harder!</button>
                                    <button type="button" class="btn btn-success btn-lg btn-block collapse" id="flagTrue"><i class="fa fa-smile"></i> Congratulations! You got the flag</button>

                                    <button style="display: none" href="#flagFalse" data-toggle="collapse" id="flagFalseButton"></button>
                                    <button style="display: none" href="#flagTrue" data-toggle="collapse" id="flagTrueButton"></button>
                                </div>
                                <div class="tab-pane fade" id="solves">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Username</th>
                                                    <th>Solved At</th>
                                                </tr>
                                            </thead>
                                            <tbody id="solvesBody">
                                                <tr><td colspan='3'>Be the first to solve this challenge</td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@section('includes-scripts')
    @parent

    <script src="{{ asset('2.0/assets/js/challs.js') }}"></script>
@endsection