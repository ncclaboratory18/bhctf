@extends('layouts.master')

@section('custom_css')
<link rel="stylesheet" href="/css/team.css">
@endsection

@section('content')
	<div class="container">

		<h1 class="text-center">{{ $team->name }}</h1>
		<hr>
	
		<div class="row">
			@foreach($team->user as $user)
			<div class="column">
				<div class="card">
					<img src="/images/user/{{ $user->profpic }}" alt="Jane" style="width:100%">
					<div class="container">
						<h2>{{ $user->username }}</h2>
						@if($team->user_id == $user->id)
							<p class="title">Leader</p>
						@else
							<p class="title">Member</p>
						@endif
					</div>
				</div>
			</div>
			@endforeach
		</div>
		
	</div>
@endsection