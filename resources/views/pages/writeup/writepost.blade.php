@extends('layouts.master')

@section('title' ,'Post a Writeup')

@section('content')
	<div class="row">
		<div class="col-md-9">
			<form method="POST" class="form-horizontal" action="submit">
			{{ csrf_field() }}

				@foreach(Request::all() as $name => $value)
					<input type="hidden" name="{{ $name }}" value="{{ $value }}">
				@endforeach

				<div class="form-group">
					<div class="col-lg-10">
						<textarea name="body" id="body">
							@if(isset($writeupcontent))
								{{ $writeupcontent }}
							@endif
						</textarea>
					</div>
				</div>

				<div class="form-group">
					<div class="col-lg-10">
						<input type="submit" name="submit" value="Submit" class="btn btn-primary">
					</div>
				</div>
			</form>
		</div>

		<div class="col-md-3">
			
		</div>
	</div>
@endsection()

@section('includes-scripts')
	@parent

	<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
	<script>CKEDITOR.replace('body')</script>
@endsection