@extends('layouts.master')

@section('title', 'Writeup')

@section('content')
	<!-- -->

			<h1 class="blog-post-title">{{ $challs->title }} Writeup</h1>
			<ul class="blog-post-info list-inline">
				<li>
					<a href="#">
						<i class="fa fa-user"></i> 
						<span class="font-lato">{{ $user->username }}</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-comment-o"></i> 
						<span class="font-lato">{{ $comments->count() }} Comments</span>
					</a>
				</li>
				<li>
					<i class="fa fa-heart"></i> 
					<a class="category" href="#">
						<span class="font-lato" value="{{ $writeup->like->count() }}" id="likesCount">{{ $writeup->like->count() }} likes</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-clock-o"></i> 
						<span class="font-lato">{{ $writeup->created_at->toDayDateTimeString() }}</span>
					</a>
				</li>
				<li>
					<i class="fa fa-list"></i> 
					<a class="category" href="#">
						<span class="font-lato">Category: {{ $challs->category->name }}</span>
					</a>
				</li>
			</ul>

			<!-- article content -->
			{!! $writeup->body !!}


			<div class="divider divider-dotted"><!-- divider --></div>
			
			<div class="text-center">
				@if($like->count())
				<button type="button" id="unlikeButton" value="{{ $writeup->id }}" class="btn btn-lg btn-3d btn-teal"><i class="fa fa-heart-o"></i> Unlike</button>
				@else
				<button type="button" id="likeButton" value="{{ $writeup->id }}" class="btn btn-lg btn-3d btn-red"><i class="fa fa-heart"></i> Like This Writeup to Say Thanks</button>
				@endif
			</div>
	
			<div class="divider"><!-- divider --></div>

			
			<!-- COMMENTS -->
			<div id="comments" class="comments">

				<h4 class="page-header margin-bottom-60 size-20">
					<span>{{ $comments->count() }}</span> COMMENTS
				</h4>
				
				@if ($comments->count())
				@foreach ($comments as $comment)
					<!-- comment item -->
					<div class="comment-item" id="comment-{{ $comment->id }}">

						<!-- user-avatar -->
						{{-- <span class="user-avatar">
							<img class="pull-left media-object" src="assets/images/avatar.png" width="64" height="64" alt="">
						</span> --}}

						<div class="media-body">
							{{-- <a href="#commentForm" class="scrollTo comment-reply">reply</a> --}}
							<h4 class="media-heading bold">{{ $comment->user->username }}</h4>
							<small class="block">{{ $comment->created_at->toDayDateTimeString() }}</small>
							{{ $comment->body }}
						</div>
					</div>
				@endforeach
				@else
					<p>There are no comments</p>
				@endif



				<h4 class="page-header size-20 margin-bottom-60 margin-top-100">
					LEAVE A <span>COMMENT</span>
				</h4>

				<!-- Form -->
				<form action="/writeup/comment" method="post">
					{{ csrf_field() }}

					<input type="hidden" name="wid" value="{{ $writeup->id }}">
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<label>COMMENT</label>
								<textarea required="required" maxlength="5000" rows="5" class="form-control" name="body" id="comment"></textarea>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">

							<button class="btn btn-3d btn-lg btn-reveal btn-black">
								<i class="fa fa-check"></i>
								<span>SUBMIT</span>
							</button>

						</div>
					</div>

				</form>
				<!-- /Form -->

			</div>
			<!-- /COMMENTS -->

	<!-- / -->
@endsection

@section('includes-scripts')
	@parent

	<script>
		function removeDisabled(){
			setTimeout(function() {
			    $(arguments[0]).removeClass('disabled');
			    $(arguments[0]).removeAttr('title');
			}, 3000, arguments[0]);
		}
	</script>

	<script>
		$("#likeButton").click(function(){
			$.ajax({
				url: '/writeup/like',
				type: 'POST',
				data: {
					'_token': '{{ csrf_token() }}',
					'wid': $(this).val()
				},
				error: function(){

				},
				context: this,
				dataType: 'json',
				success: function(data){
					if(data)
					{
						$(this).attr('id', 'unlikeButton');
						$(this).removeClass('btn-red');
						$(this).addClass('btn-teal');
						$(this).addClass('disabled');
						$(this).attr('title', 'You can like/unlike in 10seconds interval');
						$(this).html('<i class="fa fa-heart-o"></i> Unlike');

						var likeObj = $("#likesCount");
						var like = parseInt(likeObj.attr('value'));

						likeObj.val(++like);
						likeObj.html(like + ' likes');

						removeDisabled(this);
					}
				}
			});
		});

		$("#unlikeButton").click(function(){
			$.ajax({
				url: '/writeup/unlike',
				type: 'POST',
				data: {
					'_token': '{{ csrf_token() }}',
					'wid': $(this).val()
				},
				context: this,
				error: function(){

				},
				dataType: 'json',
				success: function(data){
					if(data)
					{
						$(this).attr('id', 'likeButton');
						$(this).removeClass('btn-teal');
						$(this).addClass('btn-red');
						$(this).addClass('disabled');
						$(this).attr('title', 'You can like/unlike in 10seconds interval');
						$(this).html('<i class="fa fa-heart"></i> Like This Writeup to Say Thanks');

						var likeObj = $("#likesCount");
						var like = parseInt(likeObj.attr('value'));

						likeObj.val(--like);
						likeObj.html(like + ' likes');

						removeDisabled(this);
					}
				}
			});
		});
	</script>
@endsection