@extends('layouts.master')

@section('title', 'Writeup' )

@section('content')
	
	<div class="row">
		<div class="col-md-8">
			<h3>Writeups for {{ $challs->title }}</h3>
		</div>

		<div class="col-md-4">
			<a href="/writeup/post?cid={{ $challs->id }}&action=new" class="btn btn-info pull-right">Post Your Own Writeup</a>
		</div>

	</div>

	{{-- Writeup - Main --}}
	@if($writeup->count())
		<div class="row">
			<div class="col-lg-12">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Written By</th>
								<th>Posted At</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($writeup as $w)
								<tr>
									<td><a href="/user/{{ $w->user->username }}">{{ $w->user->username }}</a></td>
									<td>{{ $w->created_at->toDayDateTimeString() }}</td>
									<td><a class="btn btn-default btn-xs" href="/writeup/read/{{ $w->id }}"><i class="fa fa-hand-o-right"></i> Read Writeup</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@else
		<div class="row">
			<div class="col-lg-12">
				<br>
				<p>Sorry, writeup not available yet</p>
			</div>
		</div>
	@endif
@endsection()