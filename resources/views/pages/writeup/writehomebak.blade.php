@extends('layouts.master')

@section('content')

    <div class="container">

    <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Writeups
                    <br>
                    <small>Writeup Sementara Tersedia di <a href="http://blog.clayday.id"></a></small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <hr>
        <h1>Select Challenge</h1>
        <!-- Projects Row -->
        @foreach($category as $cat)
            <div class="row">
                @if($cat->challs->count())
                <div class="col-lg-12">
                    <h2>{{ $cat->name }}</h2>
                </div>
                @endif

                @foreach($cat->challs as $c)
                    <a href="writeup/show?id={{ $c->id }}">
                    @if(!in_array($c->id, $done))
                        <div class="col-md-2 portfolio-item">
                                    <h3>{{ $c->title }}</h3>
                                    <p>{{ $c->score }}</p>
                            </div>
                        </a>
                    @else
                    <div class="col-md-2 portfolio-item-completed">
                                    <h3>{{ $c->title }}</h3>
                                    <p>{{ $c->score }}</p>
                            </div>
                        </a>
                    @endif
                @endforeach()
            </div>
        @endforeach
        <hr>
    </div>

@endsection()
