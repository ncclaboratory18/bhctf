@extends('layouts.master')

@section('title', 'Writeups')

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">

    <div class="row">
        
        <div class="col-md-3 col-sm-3">
            <div class="side-nav-head">
                <h4>Recent Activities</h4>
            </div>

            <!-- JUSTIFIED TAB -->
            <div class="tabs nomargin-top hidden-xs margin-bottom-60">

                <!-- tabs -->
                <ul class="nav nav-tabs nav-bottom-border nav-justified">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab">
                            Writeups
                        </a>
                    </li>
                    <li>
                        <a href="#tab_2" data-toggle="tab">
                            Comments
                        </a>
                    </li>
                </ul>

                <!-- tabs content -->
                <div class="tab-content margin-bottom-60 margin-top-30">
                    

                    <!-- POPULAR -->
                    <div id="tab_1" class="tab-pane active">
                        
                        @foreach ($writeups as $writeup)
                        <div class="row tab-post"><!-- post -->
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <i class="fa fa-plus-square"></i>
                            </div>


                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <a href="/writeup/read/{{ $writeup->id }}" class="tab-post-link"> {{ $writeup->user->username }} posted a writeup for {{ $writeup->challs->title }} ({{ $writeup->challs->category->name }})</a>
                                <small>{{ $writeup->created_at->toDayDateTimeString() }}</small>
                            </div>
                        </div><!-- /post -->
                        @endforeach

                    </div>
                    <!-- /POPULAR -->


                    <!-- RECENT -->
                    <div id="tab_2" class="tab-pane">

                        @foreach ($comments as $comment)
                        <div class="row tab-post"><!-- post -->
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <i class="fa fa-commenting"></i>
                            </div>


                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <a href="/writeup/read/{{ $comment->writeup->id }}#comment-{{ $comment->id }}" class="tab-post-link"> {{ $comment->user->username }} said: {{ str_limit(strip_tags($comment->body), 20)  }}...</a>
                                <small>{{ $comment->created_at->toDayDateTimeString() }}</small>
                            </div>
                        </div><!-- /post -->
                        @endforeach
                    </div>

                </div>

            </div>
            <!-- JUSTIFIED TAB -->


        </div>


        <div class="col-md-9 col-sm-9">
            @if ($conid)
                <h1><span class="label label-default">{{ $cname }}</span></h1>
            @else
                <h1>Select Challenge</h1>
            @endif
            @foreach ($category as $cat)
                <div class="row">
                    <div class="col-md-12">
                        @if ($cat->challs($conid)->count() > 0)
                            <div class="heading-title heading-dotted text-center">
                                <h3><span>{{ $cat->name }}</span></h3.1>
                            </div>
                        
                            @foreach ($cat->challs($conid) as $chall)
                                @if (in_array($chall->id, $done))
                                <a href="/writeup/show/{{ $chall->id }}" class="btn btn-3d btn-xlg btn-green challs completed">
                                    {{ $chall->title }}
                                    <span class="block font-lato">{{ $chall->score }}</span>
                                </a>
                                @else
                                <a href="/writeup/show/{{ $chall->id }}" class="btn btn-3d btn-xlg btn-brown challs" id="{{ $chall->id }}">
                                    {{ $chall->title }}
                                    <span class="block font-lato">{{ $chall->score }}</span>
                                </a>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <br>
            @endforeach
        </div>
        
    </div>
@endsection

@section('includes-scripts')
    @parent

    <script src="{{ asset('2.0/assets/js/challs.js') }}"></script>
@endsection