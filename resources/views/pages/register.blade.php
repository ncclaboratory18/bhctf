@extends('layouts.master')contests

@section('title', 'Register')

@section('content')
<div class="row">
	<div class="col-md-6 col-md-offset-3">

		<div class="box-static box-border-top padding-30">
			<div class="box-title margin-bottom-30">
				<h2 class="size-20">Register </h2>
			</div>

			<form class="nomargin" method="post" action="/register" autocomplete="off" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="clearfix">
					
					<div class="form-group">
						<input type="text" name="username" class="form-control" placeholder="Username" required="" value="{{ old('username') }}" autofocus="autofocus">
					</div>

					<div class="form-group">
						<input type="text" name="nrp" class="form-control" placeholder="NRP" required="" value="{{ old('nrp') }}">
					</div>

					<div class="form-group">
						<input type="email" name="email" class="form-control" placeholder="Email" required="" value="{{ old('email') }}">
					</div>
					
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Password" required="" >
					</div>

					<div class="form-group">
						<input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required="">
					</div>
					
					<div class="form-group">
						<label style="color: white">KTM</label>
						<div class="fancy-file-upload fancy-file-primary">
							<i class="fa fa-upload"></i>
							<input type="file" class="form-control" name="ktm" onchange="jQuery(this).next('input').val(this.value);" />
							<input type="text" class="form-control" placeholder="no file selected" />
							<span class="button">Choose File</span>
						</div>
						<small class="text-muted block" style="color: white">Max file size: 2MB (jpeg/jpg/png)</small>
					</div>

				</div>
				
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-right">
						<button class="btn btn-primary">REGISTER</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection
