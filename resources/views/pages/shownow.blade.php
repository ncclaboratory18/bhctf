@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="col-lg-6">
			<h1>{{ $obj->title }}</h1>
			<h5 style="font-style: italic;">Category: {{ $obj->category->name }}</h5>
			<hr>
			{!! $obj->desc !!}


			@foreach($obj->attachments as $a)
			<p><a href="download?id={{ $a->id }}">{{ $a->filename }}</a></p>
			@endforeach

			@if(!$completed)
			<form class="form-horizontal" method="POST" action="/challs/submit/{{ $obj->id }}" autocomplete="off">
				{{ csrf_field() }}

				<div class="form-group">
					<label for="ans" class="control-label">Flag:</label>
					<input type="text" class="form-control" name="ans" id="ans">
				</div>

				<div class="form-group">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</form>
			@else
			<div class="form-group">
				<div class="alert alert-success">
					<ul>
						<li>You already solved this, noob -_-</li>
					</ul>
				</div>
			</div>
			<hr>
			<p>Source: {{ $obj->source }}</p>
			@endif
		</div>

		<div class="col-lg-6">
			<h1>Lucky Noobies</h1>
			@if($completedcount)
				<table class="table table-striped table-hover">
					<tr>
						<th>No</th>
						<th>Username</th>
						<th>Solved at</th>
					</tr>
						@foreach($obj->completed as $o)
							@if(!$o->user->isAdmin())
								<tr>
									<td>{{ ++$rank }}</td>
									<td><a href="/user/{{ $o->user->username }}">{{ $o->user->username }}</a></td>
									<td>{{ $o->created_at->toDayDateTimeString() }}</td>
								</tr>
							@endif
						@endforeach
				</table>
			@else <p>Be the first to complete this challenge</p>
			@endif
		</div>
	</div>
@endsection()
