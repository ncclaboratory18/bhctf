@if (count($errors))
	<div class="container">
		<div class="form-group">
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $err)
						<li>{{ $err }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
@endif

@if (session('status'))
	<div class="container">
		<div class="form-group">
			<div class="alert alert-success">
				<ul>
					<li>{{ session('status') }}</li>
				</ul>
			</div>
		</div>
	</div>
@endif