<!DOCTYPE html>
<html>

<head>
    <meta name="googlebot" content="noindex">
    <meta name="robots" content="noindex">
    @include('admin.sections.header')
    @yield('custom_css')
</head>

<body class="theme-red">

    @include('layouts.errors')
    <!-- Page Loader -->
    @include('admin.sections.pageloader')
    <!-- #END# Page Loader -->

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Search Bar -->
    @include('admin.sections.search')
    <!-- #END# Search Bar -->

    <!-- Top Bar -->
    @include('admin.sections.topbar')
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        @include('admin.sections.leftbar')
        <!-- #END# Left Sidebar -->

        <!-- Right Sidebar -->
        @include('admin.sections.rightbar')
        <!-- #END# Right Sidebar -->
    </section>

    @yield('content')

    @include('admin.sections.scripts')

    @yield('custom_scripts')
</body>

</html>