<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>@yield('title') | BrainHack CTF</title>
		<meta name="keywords" content="HTML5,CSS3,Template" />
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
		@section('includes-css')
			<!-- CORE CSS -->
			<link href="{{ asset('2.0/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
			
			<!-- THEME CSS -->
			<link href="{{  asset('2.0/assets/css/essentials.css') }}" rel="stylesheet" type="text/css" />
			<link href="{{ asset('2.0/assets/css/layout.css')}}" rel="stylesheet" type="text/css" />

			<!-- PAGE LEVEL SCRIPTS -->
			<link href="{{ asset('2.0/assets/css/header-1.css')}}" rel="stylesheet" type="text/css" />
			<link href="{{ asset('2.0/assets/css/color_scheme/green.css') }}" rel="stylesheet" type="text/css" id="color_scheme" />
			<link id="css_dark_skin" href="{{ asset('2.0/assets/css/layout-dark.css')}}" rel="stylesheet" type="text/css" title="dark">
		@show
	</head>

	<body class="smoothscroll enable-animation">

		<!-- wrapper -->
		<div id="wrapper">

			<div id="header" class="clearfix sticky">
				<!-- TOP NAV -->
				@include('templates.topnav')
				<!-- /Top Nav -->
			</div>

			<section class="page-header page-header-xs">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h1>@yield('title')</h1>
						</div>

						<div class="col-md-9">
							@if (count($errors))
								@foreach ($errors->all() as $err)
									<span class="label label-danger"><i class="fa fa-warning"></i> {{ $err }}</span>
								@endforeach
							@endif
						</div>
					</div>
					
					
				</div>
			</section>
			<!-- /PAGE HEADER -->

			<section>
				<div class="container">
					@yield('content')
				</div>
			</section>


			<!-- FOOTER -->
			@include('templates.footer')
			<!-- /FOOTER -->

		</div>
		<!-- /wrapper -->

		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->


		<!-- JAVASCRIPT FILES -->
		@section('includes-scripts')
			<script type="text/javascript">var plugin_path = '/2.0/assets/plugins/';</script>
			<script type="text/javascript" src="{{ asset('2.0/assets/plugins/jquery/jquery-2.2.3.min.js')}}"></script>

			<script type="text/javascript" src="{{ asset('2.0/assets/js/scripts.js')}}"></script>
			
			@include('templates.notifications')
		@show
	</body>
</html>