<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\TeamContest;
use App\Contest;
use Carbon\Carbon;
class RankController extends Controller
{
	public function index($id = null)
	{
		$rank = 0;
		if($id)
		{
			
				$data['teams'] = TeamContest::where('contest_id', $id)
								->orderBy('score', 'desc')
								->orderBy('updated_at','asc')
								->paginate(20);
				// dd($data['teams']);	
				if(request()->has('page')){
					$rank = 10 * (request('page') - 1);
				}
	
				$data['rank'] = $rank;
				$data['conid'] = $id;
			
			

			return view('pages.contest.contest_rank', $data);
		}
		else
		{
			$teams = DB::table('users')->where('permission', 0)->where('status', 0)
					->orderBy('score', 'desc')
					->paginate(20);

			if(request()->has('page'))
			{
				$rank = 10 * (request('page') - 1);
			}

			return view('pages.rank', compact("teams", "rank"));
		}


	}

	public function index2($id = null)
	{
		$rank = 0;
		if($id)
		{
				// $contest = Contest::select('score','score_add')
				// 			->where('contest_id', $id)
				// 			->orderBy('score', 'desc')
				// 			->orderBy('updated_at','asc')
				// 			->paginate(20);
				// $data['teams'] = DB::table('team_contests')->select(DB::raw('* ,score + score_add  as total'))
				// 		->orderBy('total', 'desc')
				// 		->orderBy('updated_at','asc')
				// 		->paginate(20);

				$data['teams'] = TeamContest::selectRaw('* ,score + score_add  as total')
						->where('contest_id', $id)
						->orderBy('total', 'desc')
						->orderBy('updated_at','asc')
						->paginate(20);
						// ->get();
				// dd($data['teams']);	
				// $data['teams'] = TeamContest::where('contest_id', $id)
				// 				->orderBy('score', 'desc')
				// 				->orderBy('updated_at','asc')
				// 				->paginate(20);
	
				if(request()->has('page')){
					$rank = 10 * (request('page') - 1);
				}
	
				$data['rank'] = $rank;
				$data['conid'] = $id;
			
			

			return view('pages.contest.contest_final_rank', $data);
		}
		else
		{
			$teams = DB::table('users')->where('permission', 0)->where('status', 0)
					->orderBy('score', 'desc')
					->paginate(20);

			if(request()->has('page'))
			{
				$rank = 10 * (request('page') - 1);
			}

			return view('pages.rank', compact("teams", "rank"));
		}


	}
}
