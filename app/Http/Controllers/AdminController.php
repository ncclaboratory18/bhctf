<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\Admin;
use App\User;
use App\Contest;
use App\TeamContest;
use App\Team;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(Admin::class, ['except' => ['index', 'logadm']]);
    }

    public function index()
    {
        if(auth()->check())
        {
            return redirect()->home();
        }

    	return view('admin.sulogin');
    }

    public function logadm()
    {
        if (!auth()->attempt(request(['username', 'password'])))
        {
            if(!user()->permission) 
            {
                return redirect()->home();
            }
        }
        return redirect('/21232f297a57a5a743894a0e4a801fc3/pages/dashboard');
    }

    public function view($page)
    {
        if ($page === 'challs-organizer')
        {
            $challs = \App\Challs::orderBy('contest_id', 'asc')->get();
            $category = \App\Category::all();
            $contest = \App\Contest::all();

            return view('admin.pages.'.$page, compact("challs", "category", "contest"));
        }

        if ($page === 'user-organizer')
        {
            $users = User::where('permission', 0)->get();
            $num = 0;

            return view('admin.pages.'.$page, compact("users", "num"));
        }

        if ($page === 'contest-organizer')
        {
            $contest = Contest::all();

            return view('admin.pages.'.$page, compact("contest"));
        }


        return view('admin.pages.'.$page);
    }

    public function create(Request $request)
    {
        $challs = new \App\Challs;

        $challs->title = request('title');
        $challs->desc = request('desc');
        $challs->category_id = request('category');
        $challs->score = request('score');
        $challs->fakinflag = request('flag');
        $challs->source = request('source');
        $challs->contest_id = request('contest_id');

        $challs->save();

        if($file = $request->file('attach'))
        {
            $file->move(base_path() . '/public/attachments/', $file->getClientOriginalName());

            $attach = new \App\Attachments;
            $attach->challs_id = $challs->id;
            $attach->filename = $file->getClientOriginalName();
            $attach->dlink = public_path('attachments').'/'.$file->getClientOriginalName();

            $attach->save();
        }


        return back();
    }

    public function changestat()
    {
        $sel = User::find(request()->input('id'));
        $sel->status = 0;


        // Sending mail
        // \Mail::send('emails.welcome', $sel, function($message) use ($sel) 
        // {
        //     $message->to('hero.lionel10@gmail.com');
        //     $message->subject('Mailgun Testing');
        // });
        
        // dd('Mail Send Successfully');
        $sel->save();

        return back();
    }

    public function download()
    {
        $name = request()->input('nrp');
        return response()->download('../storage/images/'.$name.'.jpg');
    }

    public function deluser()
    {
        $deluser = User::find(request()->input('id'));
        $deluser->delete();
        return back();
    }

    public function newnews()
    {
        $news = new \App\News;

        $news->title = request('title');
        $news->poster = request('poster');
        $news->content = request('content');

        $news->save();

        return back();
    }

    public function delete($id)
    {
        \App\Challs::destroy($id);

        return back();
    }

    public function edit($id)
    {
        $challs = \App\Challs::find($id);
        $category = \App\Category::all();

        return view('admin.pages.edit', compact('challs', 'category'));
    }

    public function change($id)
    {
        $challs = \App\Challs::find($id);

        $challs->title = request('title');
        $challs->desc = request('desc');
        $challs->category_id = request('category');
        $challs->score = request('score');
        $challs->fakinflag = request('flag');
        $challs->source = request('source');
        $challs->contest_id = request('contest_id');

        $challs->save();

        return redirect('/21232f297a57a5a743894a0e4a801fc3/pages/challs-organizer')->with('Challenge Edited');
    }

    public function kontes()
    {
        $contest = new \App\Contest;

        $contest->name = request('name');
        $contest->starts_at = request('starts_at');
        $contest->ends_at = request('ends_at');

        $contest->save();

        return back();
    }
}
