<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LoginController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'destroy']);
	}

    public function index()
    {
    	return view('pages.login');
    }

    public function logmein()
    {
        // coba login
    	if (!auth()->attempt(request(['username', 'password'])))
    	{
    		return back()->withErrors([
                'message' => 'Wrong username and password'
            ]);
    	}

        $user = auth()->user();

        // cek apa sudah dikonfirmasi
        // if ($user->status == 1)
        // {
        //     auth()->logout();
        //     return back()->with(
        //         'status', 'You have been registered, please wait for admin confirmation. You will be notified by email. If you have not been confirmed in 1x24h, contact admin at brainhackadm@gmail.com'
        //     );
        // }
        // cek apa tidak di banned
        if ($user->status == 3)
        {
            auth()->logout();
            return back()->withErrors([
                'message' => 'Your account have been banned. Contact admin at NCC if you want it back'
            ]);
        }

        // login
    	return redirect()->route('challs.index')->with('status', 'Welcome back!');
    }

    public function send()
    {
        $user = \App\User::where('username', request('username'));

        if($user->count())
        {
            $user = $user->first();

            \Mail::send('emails.forgot', ['user' => $user], function($message) use ($user){
                $message->from('brainhackadm@gmail.com', 'BrainHack Admin');
                $message->to($user->email);
                $message->subject('Forgot Password');
            });

            return back()->with('status', 'Please check your email to reset your password');
        }
        else return back()->withErrors(['message' => 'The username you entered doesn\'t exists']);
    }

    public function resetview()
    {
        return view('pages.forgotpassword');
    }

    public function reset()
    {
        $user = \App\User::find(request('i'));

        if($user->user_token === request('token') && $user->count())
        {
            $this->validate(request(), [
                'password' => 'required|confirmed|min:8|max:40',
            ]);

            $user->password = bcrypt(request('password'));
            $user->user_token = base64_encode(Str::random(20));
            $user->save();

            return redirect('/login')->with('status', 'Password resetted, you can now login with your new password');
        }
        else return back()->withErrors(['message' => 'Your request is invalid.']);
    }

    public function destroy()
    {
    	auth()->logout();
        request()->session()->flush();

    	return redirect()->home();
    }
}
