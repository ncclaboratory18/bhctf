<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\TeamUser;
use DB;
use App\TeamContest;
use App\TeamCompleted;
use App\TeamAchivement;
use Illuminate\Support\Str;

class TeamController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
    	$user = auth()->user();
    	$team = $user->team;

    	return view('pages.contest.team', compact('team', 'user'));
    }

    public function info($name)
    {
        $team = Team::where('name', $name)->first();
        if($team == null) return abort(404);

        return view('pages.teamview', compact('team'));
    }

    public function exit($id)
    {
    	$team = Team::find($id);

        $team->user()->detach(auth()->user()->id);

    	return redirect('/team')->with('status', 'Success');
    }

    public function join()
    {
    	$token = request('token');

    	if($team = Team::where(DB::raw('BINARY `token`'), $token)->first())
    	{
    		if($team->teamuser->where('user_id', auth()->user()->id)->count())
    			return back()->withErrors('You are already a member of '.$team->name);

            $team->user()->attach(auth()->user()->id);

    		return back()->with('status', 'You joined '.$team->name);
    	}
    	else
    	{
    		return back()->withErrors('The token you are entered is invalid');
    	}
    }

    public function create()
    {
    	$this->validate(request(), [
    		'team_name' => 'required|unique:teams,name|max:25',
    	]);

    	$team = new Team;

    	$team->name = request('team_name');
    	$team->user_id = auth()->user()->id;
    	$team->token = $team->name.'-'.Str::random(6);

    	$team->save();

        $team->user()->attach(auth()->user()->id);

    	return back()->with('status', 'Success');
    }

    public function delete($id)
    {
    	$team = Team::find($id);
        $teamcontest = TeamContest::where('team_id', $id);
        $teamcompleted = TeamCompleted::where('team_id', $id);

        $teamcontest->delete();
        $teamcompleted->delete();
    	$team->user()->detach();
    	$team->delete();

    	return back()->with('status', 'Team deleted');
    }
}
