<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Challs;
use App\Attachments;
use App\Completed;
use App\TeamCompleted;
use App\TeamContest;
use App\Category;
use App\User;
use App\Team;
use App\Contest;
use Carbon\Carbon;

class ChallsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $cid = 0)
    {
        if(request()->session()->has('current_contest')){
            return redirect()->route('contest.index');
        }

        $category = Category::where('name', '!=', 'broken')->get();
        $user = auth()->user();
        $done = Completed::where('user_id', $user->id)->pluck('challs_id')->toArray();

        if($cid)
        {
            $conid = request('cid');
            $contest = Contest::find($conid);

            if($contest->ends_at >= Carbon::now('Asia/Jakarta'))
                return redirect('/contest')->with('error', 'Contest not over yet..');
        }
        else
            $conid = 0;

        $top10 = User::where('permission', '0')->limit(10)->orderBy('score', 'desc')->get();
        $no = 1;

        return view('pages.challs', compact("category", "done", "conid", "top10", "no"));
    }

    public function show()
    {
        //select chall
        $id = request()->input('id');
        $obj = Challs::find($id);

        if($obj->contest_id != 0)
            if($obj->contest->ends_at >= Carbon::now('Asia/Jakarta')) return back();

        //is user completed this chall?
        $user = auth()->user();
        $completed = $user->completed->where('user_id', $user->id)->where('challs_id', $id)->count();

        $rank = 0;
        //get all user who completed this chall
        $completedcount = $obj->completed->count();
        return view('pages.shownow', compact("obj", "completed", "rank", "completedcount"));
    }

    public function downloader($id)
    {
        $link = Attachments::find($id);

        return response()->download($link->dlink);
    }

    public function submit()
    {
        $id = request('id');
        $ans = request('ans');
        $challs = Challs::find($id);

        if($challs->contest_id != 0){
            $cont= request()->session()->get('current_contest');
            if($cont==null){
                return $this->submitPastContestFlag($id, $ans);
            }else{
                return $this->submitContestFlag($id, $ans);

            }
        }

        $user = auth()->user();


        if ($user->completed->where('user_id', $user->id)->where('challs_id', $challs->id)->count())
        {
            return 'Error Exception';
        }

        if ($challs->fakinflag === $ans)
        {
            if($challs->contest_id == 0)
            {
                $user->score += $challs->score;
                $user->save();
            }

            $completed = new Completed;
            $completed->user_id = $user->id;
            $completed->challs_id = $challs->id;
            $completed->save();

            return response()->json(1);
        }
        return response()->json(0);
    }

    public function submitContestFlag($id, $ans)
    {
        $teamid = request()->session()->get('team_id');
        $contest = request()->session()->get('current_contest');
        $Newcontest = Contest::find($contest->id);
        session(['current_contest' => $Newcontest]);
        if($Newcontest->ends_at < Carbon::now('Asia/Jakarta'))
            return redirect('/contest/exit');   
            
        $challs = Challs::find($id);
        $team = Team::find($teamid);
        
        if ($team->teamcompleted->where('challs_id', $challs->id)->count())
        {
            // dd("asass");
            return response()->json(2);
        }
        
        if ($challs->fakinflag === $ans)
        {
            $new = new TeamCompleted;
            
            $new->team_id = $teamid;
            $new->challs_id = $id;
            
            $tim = TeamContest::where('team_id', $teamid)->where('contest_id', $Newcontest->id)->first();
            // dd($Newcontest->freezed_at, Carbon::now('Asia/Jakarta'));
            if($Newcontest->freezed_at < Carbon::now('Asia/Jakarta')){
                // dd("Beku");
                $tim->score_add += $challs->score;

            }else{
                // dd("Update");
                $tim->score += $challs->score;
            }
            // dd("a");
            $tim->save();
            $new->save();

            return response()->json(1);
        }
        return response()->json(0);
    }
    public function submitPastContestFlag($id, $ans)
    {
        $userid = auth()->user();
        $contest = request()->session()->get('view_contest');
        $Newcontest = Contest::find($contest->id);   
        $challs = Challs::find($id);
        if ($userid->completed->where('challs_id', $challs->id)->count())
        {
            return response()->json(2);
        }
        
        if ($challs->fakinflag === $ans)
        {
            $new = new Completed;
            $new->challs_id = $id; 
            $new->user_id = $userid->id;
            $new->save();

            return response()->json(1);
        }
        return response()->json(0);
    }

    public function getChalls()
    {
        $challs = Challs::find(request('id'), ['id', 'title', 'desc', 'score']);
        $challs->attach = $challs->attachments->toArray();

        // var_dump($challs);
        // die();

        return response()->json($challs);
    }

    public function getSolves()
    {
        $challs = Challs::find(request('id'));

        if($challs->contest_id != 0)
        {
            $completed = TeamCompleted::where('team_id', '!=', 0)
                ->where('challs_id', request('id'))
                ->with('team')
                ->orderBy('created_at')
                ->get()
                ->toArray();
        }
        else
        {
            $completed = Completed::where('challs_id', request('id'))->
                            with(['user' => function($query){
                                $query->where('permission', '!=', 1);
                            }])
                            ->orderBy('created_at')
                            ->get()
                            ->toArray();
        }

        return response()->json($completed);
    }
}
