<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\AlertMail;
use App\User;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('permission', 0)->get();
        $num = 0;

        return view('admin.pages.users-organizer', compact("users", "num"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Alert user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->user_id == 'all')
        {
            $user = User::where('permission', 0)->where('status', 0)->where('subscription', 1)->get();

            foreach($user as $u)
            {
                $data = [
                    'user' => $u,
                    'subject' => $request->title,
                    'body' => $request->content
                ];

                \Mail::to($u->email)->queue(new AlertMail($data));
            }


        }
        else
        {
            $user = User::find($request->user_id);

            \Mail::send('emails.alert', ['title' => $request->title, 'body' => $request->content, 'user' => $user], function($message) use($request, $user) {
                $message->from('brainhackadm@gmail.com', 'Brainhack Admin');
                // $message->to('hero.lionel@gmail.com');
                $message->to($user->email);
                $message->subject($request->title);
            });
        }


        return back();
    }

    /**
     * Show KTM
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('admin.pages.ktm', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Ban/unban the user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if(request('action') == 'ban')
            $user->status = 3;
        else if(request('action') == 'unban')
            $user->status = 0;

        $user->save();

        return response()->json(array('status' => 'success'));
    }

    public function verify()
    {
        $id = request('id');

        $user = User::find($id);
        $user->status = 0;

        $user->save();

        \Mail::send('emails.registered', ['user' => $user], function($message) use($user){
            $message->from('brainhackadm@gmail.com', 'Brainhack Admin');
            $message->to($user->email);
            $message->subject('Welcome to BrainHack CTF');
        });

        return response()->json(array('status' => 'success'));
    }

    public function unsubscribe()
    {
        $user = User::where('user_token', request('token'));

        if($user->count())
        {
            $user = $user->first();
            $user->subscription = 0;
            $user->user_token = base64_encode($user->nrp.Str::random(20));

            $user->save();

            return view('pages.unsubscribed');
        }
        else return abort(500);
    }

    public function newktm()
    {
        if(!auth()->check())
            return redirect('/login')->withErrors(['message' => 'You must login first']);

        return view('pages.newktm');
    }

    public function upnewktm(Request $request)
    {
        $user = auth()->user();

        $this->validate(request(), [
            'ktm' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imagename = $user->nrp.'.'.$request->file('ktm')->getClientOriginalExtension();
        $request->file('ktm')->move(base_path() . '/storage/app/public/images/', $imagename);

        $user->ktm = $imagename;
        $user->save();

        return redirect('/')->with('status', 'Successfully updated KTM');
    }

    public function generate()
    {
        $user = User::all();

        foreach($user as $u)
        {
            $u->user_token = base64_encode($u->nrp.Str::random(20));
            $u->save();
        }

        return back();
    }
}
