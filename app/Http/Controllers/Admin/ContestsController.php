<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contest;
use Carbon\Carbon;

class ContestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contest = Contest::all();

        return view('admin.pages.contests-organizer', compact("contest"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contest = new Contest;
        $contest->name = $request->name;
        $contest->starts_at = Carbon::parse($request->starts_at);
        $contest->ends_at = Carbon::parse($request->ends_at);
        $contest->freezed_at = Carbon::parse($request->freezed_at);

        $contest->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $conts = Contest::find($id);
        $resp = $conts->toArray();
        $dateStarts= date_create($resp['starts_at']);
        $dateEnds= date_create($resp['ends_at']);
        $dateFreeze=date_create($resp['freezed_at']);
        $resp['starts_at']=$dateStarts->format('Y-m-d\TH:i:s');
        $resp['ends_at']=$dateEnds->format('Y-m-d\TH:i:s');
        $resp['freezed_at']=$dateFreeze->format('Y-m-d\TH:i:s');
        return response()->json($resp);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $contest = Contest::findOrFail($id);
        $resp['name'] = $request->name;
        $resp['starts_at'] = Carbon::parse($request->starts_at);
        $resp['ends_at'] = Carbon::parse($request->ends_at);
        $resp['freezed_at'] = Carbon::parse($request->freezed_at);
        $contest->update($resp);
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
