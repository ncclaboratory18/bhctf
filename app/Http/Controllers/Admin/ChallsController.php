<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Attachments;
use App\Challs;
use App\Category;
use App\Contest;

class ChallsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $challs = Challs::orderBy('contest_id', 'asc')->get();
        $category = Category::all();
        $contest = Contest::all();

        return view('admin.pages.challs-organizer', compact('challs', 'category', 'contest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $challs = new \App\Challs;

        $challs->title = $request->title;
        $challs->desc = $request->desc;
        $challs->category_id = $request->category;
        $challs->score = $request->score;
        $challs->fakinflag = $request->flag;
        $challs->source = $request->source;
        $challs->contest_id = $request->contest;

        $challs->save();

        if($file = $request->file('attach'))
        {
            $file->move(storage_path() . '/attachments/', $file->getClientOriginalName());

            $attach = new Attachments;
            $attach->challs_id = $challs->id;
            $attach->filename = $file->getClientOriginalName();
            $attach->dlink = storage_path('attachments').'/'.$file->getClientOriginalName();

            $attach->save();
        }


        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $challs = Challs::find($id);
        // $resp = $challs->toArray();
        // $resp['fakinflag'] = $challs->fakinflag;
        // var_dump($resp);
        // die();
        return response()->json($challs->makeVisible('fakinflag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Challs::findOrFail($id);
        $user->update($request->except(['_token', '_method', 'attach']));

        if($file = $request->file('attach'))
        {
            $attachments = Attachments::where('challs_id', $id)->get();
            foreach ($attachments as $key => $value) {
                $delete = Storage::disk('attachments')->delete($value->filename);
                $value->delete();
            }

            $file->move(storage_path() . '/attachments/', $file->getClientOriginalName());

            $attach = new Attachments;
            $attach->challs_id = $id;
            $attach->filename = $file->getClientOriginalName();
            $attach->dlink = storage_path('attachments').'/'.$file->getClientOriginalName();

            $attach->save();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $challs = Challs::find($id);
        $challs->delete();

        return response()->json(array('status' => 'success'));
    }
}
