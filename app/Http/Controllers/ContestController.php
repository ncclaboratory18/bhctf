<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Contest;
use App\Category;
use App\Completed;
use App\TeamContest;
use App\TeamUser;
use App\Team;
use App\Challs;
use App\TeamCompleted;

class ContestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	if(request()->session()->has('current_contest'))
        {
            $contest = request()->session()->get('current_contest');
            if($contest->ends_at < Carbon::now('Asia/Jakarta'))
                return redirect('/contest/exit');

            $team = Team::find(request()->session()->get('team_id'));

            $data['category'] = Category::all();
            $data['done'] = TeamCompleted::where('team_id', $team->id)->pluck('challs_id')->toArray();
            $data['conid'] = $contest->id;
            $data['contest'] = $contest;
            $data['no'] = 1;
            $data['top10'] = TeamContest::where('contest_id', $contest->id)->orderBy('score', 'desc')->orderBy('updated_at','asc')->limit(10)->get();

            return view('pages.contest.contest_challs', $data);
        }
        else
        {
            $now = Carbon::now('Asia/Jakarta');
            $up = Contest::where('ends_at', '>=', $now)->where('starts_at', '>', $now)->get();
            $past = Contest::where('ends_at', '<', $now)->get();
            $run = Contest::where('ends_at', '>=', $now)->where('starts_at', '<=', $now)->get();

            $team = auth()->user()->team->count();

    		return view('pages.contest.contest_list', compact('up', 'past', 'run', 'team', 'now'));
    	}
    }

    public function enter()
    {
        $cid =request('cid');
        $contest = Contest::find($cid);

        //dd($contest->starts_at, Carbon::now('Asia/Jakarta'));

        if($contest->starts_at > Carbon::now('Asia/Jakarta'))
            return redirect('/contest')->with('error', 'Contest not started yet');
        if($contest->ends_at < Carbon::now('Asia/Jakarta'))
            return redirect('/contest');

        $tid = request('tid');
        $tc = TeamContest::where('team_id', $tid)->where('contest_id', $cid);

        if(!$tc->count())
        {
            $new = new TeamContest;
            $new->team_id = $tid;
            $new->contest_id = $cid;
            $new->score = 0;
            $new->score_add = 0;

            $new->save();
        }

        session(['current_contest' => $contest, 'team_id' => $tid]);

        return redirect('/contest');
        // session(['team_id' => $tid]);
    }


    public function submit($id)
    {
        // $teamid = request()->session()->get('team_id');
        // $contest = request()->session()->get('current_contest');
        // $Newcontest = Contest::find($contest->id);
        if(Contest::find(request()->session()->get('current_contest'))->ends_at < Carbon::now('Asia/Jakarta'))
            return redirect('/contest/exit');

        $challs = Challs::find($id);
        $ans = request()->ans;
        $team = Team::find(request()->session()->get('team_id'));

        if ($team->teamcompleted->where('challs_id', $challs->id)->count())
        {
            return 'Error Exception';
        }

        if ($challs->fakinflag === $ans)
        {
            $new = new TeamCompleted;

            $new->team_id = request()->session()->get('team_id');
            $new->challs_id = $id;


            $tim = TeamContest::where('team_id', request()->session()->get('team_id'))->where('contest_id', request()->session()->get('current_contest'))->first();

            $tim->score += $challs->score;
            $tim->save();
            $new->save();

            return back()->with(
                'status', 'Congratulation, you got the flag! (but still noob)'
            );
        }
        return back()->withErrors([
            'message' => 'Try again noob :('
        ]);
    }

    public function result($id)
    {
        $teams = TeamContest::where('contest_id', $id)->orderBy('score', 'desc')->get();
        $rank = 0;

        return view('pages.contest.contest_rank', compact('teams', 'rank'));
    }

    public function exit()
    {
        request()->session()->forget('current_contest');

        return redirect('contest');
    }
}
