<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Writeup;
use App\Like;
use App\Category;
use App\Challs;
use App\Contest;
use App\Completed;
use App\Comment;
use Carbon\Carbon;

class WriteupController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
    	$category = Category::all();
        $user = auth()->user();
        $done = Completed::where('user_id', $user->id)->pluck('challs_id')->toArray();
        if(request()->has('cid')) 
        {
            $conid = request('cid');
            $contest = Contest::find($conid);
            if ($contest->ends_at > Carbon::now('Asia/Jakarta'))
                return redirect('/contest')->with('error', 'Request Invalid');
            $cname = $contest->name;
        }
        else
        {
            $cname = '';
            $conid = 0;
        }

        $writeups = Writeup::orderBy('created_at', 'desc')->limit(10)->get();
        $comments = Comment::orderBy('created_at', 'desc')->limit(10)->get();

        return view('pages.writeup.writehome', compact("category", "done", "conid", "writeups", "comments", "cname"));
    }

    public function show($id)
    {
        $challs = Challs::find($id);
        $expirydate = date_add($challs->created_at,date_interval_create_from_date_string("30 days"));
        $writeup = $challs->writeup;

        $completed = $challs->completed->where('user_id', auth()->user()->id);

        // dd($completed, $expirydate);

        if($completed->count() || $expirydate <= Carbon::now())
        {
    	   return view('pages.writeup.writelist', compact("challs", "writeup")); 
        }
        else return back()->withErrors([
                'message' => 'You can only see writeups of the challenge you\'ve solved. (or wait until '.$expirydate->toFormattedDateString().')'
            ]);

    }

    public function read($id)
    {
        $writeup = Writeup::find($id);
        $comments = $writeup->comment;
        $challs = $writeup->challs;
        $user = $writeup->user;
        $like = $writeup->like->where('user_id', auth()->user()->id);

        $completed = $challs->completed->where('user_id', auth()->user()->id);

        if($completed)
        {
            return view('pages.writeup.writeshow', compact("writeup", "comments", "challs", "user", "like"));
        }
        else return redirect()->home()->withErrors([
                'message' => 'You must solve the challenge first, noob -_- (or wait until '.$expirydate->toFormattedDateString().')'
        ]);
    }

    public function write()
    {
        $heading = request()->input('action');

        if($heading === 'edit')
        {
            $writeup = Writeup::find(request()->input('wid'));
            $writeupcontent = $writeup->body;

            return view('pages.writeup.writepost', compact("heading", "writeupcontent"));
        }

        return view('pages.writeup.writepost', compact("heading"));
    }

    public function submit()
    {
        $user = auth()->user();
        $action = request('action');

        if($action === 'comment')
        {
            $comment = new Comment;

            $comment->user_id = $user->id;
            $comment->writeup_id = request('wid');
            $comment->body = request('body');

            $comment->save();

            return redirect('writeup/read/'.request('wid'))->with('status', 'Comment Published');
        }
        else if ($action === 'new')
        {
            $writeup = new Writeup;

            $writeup->user_id = $user->id;
            $writeup->challs_id = request('cid');
            $writeup->body = request('body');

            $writeup->save();

           return redirect('writeup/show/'.request('cid'))->with('status', 'Writeup Published');
        }
        else if ($action === 'edit')
        {
            $writeup = Writeup::find(request('wid'));
            $writeup->body = request('body');

            $writeup->save();

           return redirect('writeup/show/'.request('cid'))->with('status', 'Edit Successful');
        }
        else return 'error';
    }

    public function comment()
    {
        $comment = new Comment;

        $comment->user_id = auth()->user()->id;
        $comment->writeup_id = request('wid');
        $comment->body = request('body');

        $comment->save();

        return redirect('writeup/read/'.request('wid'))->with('status', 'Comment Published');
    }

    public function like()
    {
        $user = auth()->user();
        $writeup = Writeup::find(request('wid'));

        // var_dump($writeup);
        // die();

        if($user->like->where('writeup_id', $writeup->id)->count())
        {
            return response()->json(0);
        }
        else
        {
            $like = new Like;
            $like->user_id = $user->id;
            $like->writeup_id = $writeup->id;

            $like->save();

            return response()->json(1);
        }
    }

    public function unlike()
    {
        $user = auth()->user();
        $writeup = Writeup::find(request('wid'));
        $like = $user->like->where('writeup_id', $writeup->id)->first();

        if($like)
        {
            $like->delete();

            return response()->json(1);
        }
        else
        {
            return response()->json(0);
        }
    }
}
