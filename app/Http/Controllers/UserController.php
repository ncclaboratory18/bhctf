<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Completed;
use App\Challs;

class UserController extends Controller
{
	public function index()
	{
		$user = auth()->user();

		return view('pages.profile', compact('user'));
	}

    public function view($name)
    {
    	$user = User::where('username', $name)->first();
    	
    	if($user->permission || $user->status) return abort(404);

    	$num = 0;
    	$totchalls = Challs::count();
    	return view('pages.userview', compact('user', 'num', 'totchalls'));
    }

    public function change()
    {
    	$user = auth()->user();

    	$this->validate(request(), [
    		'profpic' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imagename = $user->nrp . '.' .request()->file('profpic')->getClientOriginalExtension();

        $user->profpic = $imagename;
        $user->save();

        request()->file('profpic')->move(base_path() . '/public/images/user/', $imagename);

        return back()->with('status', 'Done');
    }
}