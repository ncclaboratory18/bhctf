<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use Illuminate\Support\Str;

class RegisController extends Controller
{
    public function index()
    {
    	return view('pages.register');
    }

    public function create(Request $request)
    {
    	//validate input
    	$this->validate(request(), [
            'nrp' => 'required|unique:users,nrp|min:10|max:14',
            'email' => 'required|unique:users,email|email',
    		'username' => 'required|unique:users,username',
    		'password' => 'required|confirmed|min:8|max:40',
    		'ktm' => 'required|mimes:jpeg,png,jpg|max:2048',
        ]);

        $nrp = request('nrp');
        $imagename = $nrp.'.'.$request->file('ktm')->getClientOriginalExtension();
        $request->file('ktm')->move(base_path() . '/storage/app/public/images/', $imagename);

        $arr = [
            'nrp' => $nrp,
            'email' => request('email'),
            'username' => request('username'),
            'password' => bcrypt(request('password')),
            'ktm' => $imagename,
            'user_token' => base64_encode($nrp.Str::random(20))
        ];

        //buat user baru
        $user = User::create($arr);

    	//redirect
    	return redirect('/login')->with('status', 'Thanks for registering. Don\'t forget to read the rules first!');
    }
}
