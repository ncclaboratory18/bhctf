<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamContest extends Model
{
	public function team()
	{
		return $this->belongsTo('App\Team');
	}

	public function contest()
	{
		return $this->belongsTo('App\Contest');
	}
}
