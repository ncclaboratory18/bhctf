<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Writeup extends Model
{
    public function challs()
    {
    	return $this->belongsTo('App\Challs');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function comment()
    {
    	return $this->hasMany('App\Comment');
    }

    public function like()
    {
        return $this->hasMany('App\Like');
    }
}
