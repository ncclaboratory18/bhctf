<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Contest extends Model
{
	protected $dates = ['starts_at', 'ends_at','freezed_at'];
    protected $fillable = ['name','starts_at', 'ends_at','freezed_at'];
    public function challs()
    {
    	return $this->hasMany('App\Challs');
    }

    public function teamcontest()
    {
    	return $this->hasMany('App\TeamContest');
    }
}
