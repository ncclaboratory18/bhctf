<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challs extends Model
{
    protected $fillable = [
        'title', 'desc', 'score', 'fakinflag', 'source', 'category_id', 'contest_id'
    ];

    protected $hidden = ['fakinflag'];

	public function attachments()
	{
		return $this->hasMany('App\Attachments');
	}

	public function completed()
    {
        return $this->hasMany('App\Completed');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function writeup()
    {
        return $this->hasMany('App\Writeup');
    }

    public function contest()
    {
        return $this->belongsTo('App\Contest');
    }

    public function teamcompleted()
    {
        return $this->hasMany('App\TeamCompleted');
    }
}
