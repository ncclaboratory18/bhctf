<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nrp', 'username', 'email', 'password', 'ktm', 'user_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'permission', 'status', 'user_token'
    ];

    public function isAdmin()
    {
        return $this->permission;
    }

    public function completed()
    {
        return $this->hasMany('App\Completed');
    }

    public function writeup()
    {
        return $this->hasMany('App\Writeup');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

    public function team()
    {
        return $this->belongsToMany('App\Team', 'team_users');
    }

    public function teamuser()
    {
        return $this->hasMany('App\TeamUser');
    }

    public function like()
    {
        return $this->hasMany('App\Like');
    }
}
