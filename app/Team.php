<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $visible = ['id', 'name','user_id','token','score_add'];
    protected $fillable = ['name','user_id','token','score_add'];

    public function teamcontest()
    {
    	return $this->hasMany('App\TeamContest');
    }

    public function teamuser()
    {
    	return $this->hasMany('App\TeamUser');
    }

    public function user()
    {
    	return $this->belongsToMany('App\User', 'team_users');
    }

    public function teamcompleted()
    {
        return $this->hasMany('App\TeamCompleted');
    }
}
