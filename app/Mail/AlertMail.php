<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AlertMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.alert')
                    ->with([
                        'user' => $this->data['user'],
                        'title' => $this->data['subject'],
                        'body' => $this->data['body']
                        ])
                    ->from('brainhackadm@gmail.com', 'BrainHack Admin')
                    ->subject($this->data['subject']);
    }
}
