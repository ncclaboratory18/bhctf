<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function writeup()
    {
    	return $this->belongsTo('App\Writeup');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
