<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	public function challs($id = 0)
	{
		return $this->hasMany('App\Challs')->where('contest_id', $id)->orderBy('score')->get();
	}
}
