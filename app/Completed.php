<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Completed extends Model
{
    public function challs()
    {
    	return $this->belongsTo('App\Challs');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
