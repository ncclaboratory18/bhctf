<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachments extends Model
{
	protected $visible = ['id', 'filename'];

    public function challs()
    {
    	return $this->belongsTo('App\Challs');
    }

}
