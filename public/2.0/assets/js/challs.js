
$(".challs").click(function(){
    var id = $(this).attr('id');
    $("#solvesTab").val(id);
    $("#challs_id").val(id);
    $.ajax({
        url: '/challs/getChalls',
        type: 'POST',
        data: {
            '_token': $('meta[name="_token"]').attr('content'),
            'id': id
        },
        error: function(){

        },
        dataType: 'json',
        success: function(data){
            $("#title").html(data.title);
            $("#score").html(data.score);
            $("#desc").html(data.desc);
            $("#solvesTab").val(data.id);
            var attach = '';
            $.each(data.attach, function(i, val){
                attach += '<a href="/challs/download/'+val['id']+'" class="btn btn-teal"><i class="et-download"></i> '+val['filename']+'</a>&nbsp';
            });
            $("#attachments").html(attach);
        }
    });
});



$("#solvesTab").click(function(){
    var id = $(this).val();
    $.ajax({
        url: '/challs/getSolves',
        type: 'POST',
        data: {
            '_token': $('meta[name="_token"]').attr('content'),
            'id': id
        },
        error: function(){

        },
        dataType: 'json',
        success: function(data){
            var solves = '';
            var no = 0;
            $.each(data, function(i, val){
                if(val['user'])
                    solves += '<tr> <td>'+(no+1)+'</td> <td>'+val['user']['username'] +'</td><td>'+val['created_at']+'</td></tr>';
                else if(val['team'])
                    solves += '<tr> <td>'+(no+1)+'</td> <td>'+val['team']['name'] +'</td><td>'+val['created_at']+'</td></tr>';
            });
            if(solves != '')
                $("#solvesBody").html(solves);
        }
    });
});


$("#submitFlag").click(function(){
    var id = $("#challs_id").val();
    $.ajax({
        url: '/challs/submit',
        type: 'POST',
        data: {
            '_token': $('meta[name="_token"]').attr('content'),
            'id': id,
            'ans': $("#flag").val()
        },
        error: function(){

        },
        dataType: 'json',
        success: function(status){
            if(status == 0 || status == 2)
            {
                $("#flagFalseButton").trigger('click');
                setTimeout(function() {
                    $("#flagFalseButton").trigger('click');
                }, 3000);
            }
            else if(status == 1)
            {
                $("#flagTrueButton").trigger('click');
                $("#flagForm").hide();
                $("#"+id).addClass('completed');
                $("#"+id).removeClass('btn-brown').addClass('btn-green');
                setTimeout(function() {
                    $("#flagTrueButton").trigger('click');
                    $("#solvedNotif").show();
                }, 3000);
            }
        }
    });
});


$(".completed").click(function(){
    $("#flagForm").hide();
    $("#solvedNotif").show();
});



$('#challsModal').on('hidden.bs.modal', function () {
    $("#flagForm").show();
    $("#solvedNotif").hide();
    $("a[href='#challenge']").trigger('click');
    $("#solvesBody").html("<tr><td colspan='3'>Be the first to solve this challenge</td></tr>")
})
